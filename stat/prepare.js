/**
 * Created by spirit on 01.09.17.
 */


// ^(.+):\s

var mongo = require('mongoskin');
var _ = require('underscore');
var config = require('../configs/config.js');
var fs = require('fs');
var async = require('async');

var db = mongo.db('mongodb://' + config.mongoUser + ':' + config.mongoPass + "@" + config.mongoServer + ':' + config.mongoPort + '/' + config.mongoBaseName);
var Users = db.bind('users');
var History = db.bind('history');
var Events = db.bind('historyEvents');

var getAllNicks = function (callback) {

    var nicks = {};

    Users.find({mess_count: {$gt: 100}}, {}).toArray(function(err, users){

        _.each(users, function (user) {
            nicks[user.login] = [user.nick];
        });

        Events.find({event: 8}, {}).toArray(function(err, result){
            _.each(result, function (item) {
                if(nicks[item.user]) {
                    nicks[item.user].push(item.text);
                    nicks[item.user] = _.uniq(nicks[item.user]);
                }
            });
            callback(nicks);
        });

    });

};

var getPage = function (num, cb) {
    //67
    var size = 10000;
    var skip = (num-1) * size;
    History.find({}, {limit: size, skip: skip, sort: {date: -1}}).toArray(function(err, result){
        cb(result);
    });
};

var getList = function () {

    var groups = fs.readFileSync('./stat/data/groups.json', 'utf8');
    groups = JSON.parse(groups);

    fs.writeFileSync('./stat/data/data.csv', '');

    var findGroup = function (nick) {
        var res = null;
        nick = nick.trim();
        _.each(groups, function (val, index) {
            if(val.indexOf(nick) > -1)
                res = index;
        });
        return res;
    };

    var findGroupArr = function (nicks) {
        return _.map(nicks, function (item) {
            return findGroup(item);
        });
    };

    var tasks = _.map(_.range(67), function (num) {
        return function (callback) {
            getPage(num+1, function(result){

                var items = [];
                var reg = /^(.+):\s/;

                _.each(result, function (val) {
                    var r = reg.exec(val.text);
                    if(r && val.text.indexOf('<b>') == -1) {
                        var ns = _.initial(r[0].split(': '));

                        var nsf = _.filter(ns, function (v) {
                            return _.size(v) <= 15;
                        });

                        if(_.size(nsf) > 0) {
                            // items.push({
                            //     user: val.user,
                            //     date: val.date,
                            //     nicks: nsf,
                            //     logins: findLoginByNickArr(nsf)
                            // });
                            items.push([
                                val.user == 'painter' ? 'u137130293' : val.user,
                                val.date,
                                findGroupArr(nsf).join('|')
                            ].join(';'));
                        }

                    }
                });

                console.log("page", num+1, items.length);

                fs.appendFileSync('./stat/data/data.csv', items.join('\n'));
                fs.appendFileSync('./stat/data/data.csv', '\n');

                callback(null, items);

            });
        };
    });

    async.parallelLimit(tasks, 2, function (err, result) {

        console.log("END!");

    });

};

// getAllNicks(function (res) {
//     fs.writeFile('./stat/data/allnicks.json', JSON.stringify(res), 'utf8', function (err, res) {
//         console.log("err", err);
//         console.log("res", res);
//     });
// });

function makeGroups(){

    var allNicks = fs.readFileSync('./stat/data/allnicks.json', 'utf8');
    allNicks = JSON.parse(allNicks);
    var groups = [];

    var findByNick = function (nick, ex) {
        var res = null;
        _.each(groups, function (val, key) {
            if(val.indexOf(nick) > -1)
                res = key;
        });
        return res;
    };

    _.each(allNicks, function (val, key) {
        groups.push(val);
    });

    _.each(groups, function (val, index) {
        if(val.length == 1 && val[0]) {

            var nick = val[0];

            _.each(groups, function (nicks, index2) {
                if(index != index2 && groups[index2].indexOf(nick) > -1) {
                    groups[index] = ['###' + nick];
                }
            });

        }
    });

    console.log("groups", groups);
    fs.writeFile('./stat/data/groups.json', JSON.stringify(groups), 'utf8', function (err, res) {
        console.log("err", err);
        console.log("res", res);
    });


};

//makeGroups();

getList();


// (function () {
//
//     var allNicks = fs.readFileSync('./stat/data/allnicks.json', 'utf8');
//     allNicks = JSON.parse(allNicks);
//
//     var logins = {};
//     _.each(allNicks, function (val, key) {
//
//         _.each(val, function (nick) {
//
//             if(logins[nick]) {
//                 logins[nick].push(key);
//             } else {
//                 logins[nick] = [key]
//             }
//
//         });
//
//
//     });
//
//     console.log(">>", logins);
//
//
// })();

