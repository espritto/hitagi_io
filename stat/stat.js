/**
 * Created by spirit on 05.09.17.
 */


var _ = require('underscore');
var config = require('../configs/config.js');
var mongo = require('mongoskin');
var fs = require('fs');

var db = mongo.db('mongodb://' + config.mongoUser + ':' + config.mongoPass + "@" + config.mongoServer + ':' + config.mongoPort + '/' + config.mongoBaseName);
var Users = db.bind('users');


var scv = fs.readFileSync('./stat/data/data.csv', 'utf8');
var lines = scv.split('\n');
var data = _.map(lines, function (line) {
    return line.split(';');
});
lines = null;
scv = null;

var groups = fs.readFileSync('./stat/data/groups.json', 'utf8');
groups = JSON.parse(groups);

var findGroup = function (nick) {
    var res = null;
    nick = nick.trim();
    _.each(groups, function (val, index) {
        if(val.indexOf(nick) > -1)
            res = index;
    });
    return res;
};


var relations = {};

function search(login, nick){

    var sourceUser = login;
    var sourceNick = nick;
    var _gr = findGroup(sourceNick);
    if(!_gr) return false;
    var sourceGroup = _gr.toString();

    var outGroupsCounts = {};
    var inGroupsCounts = {};

    // к кому я обращался
    _.each(data, function (item) {
        if(item[0] == sourceUser && item[2]) {

            var arr = item[2].split('|');
            _.each(arr, function (n) {
                if(!n) return false;
                if(outGroupsCounts[n]) {
                    outGroupsCounts[n]++;
                } else {
                    outGroupsCounts[n] = 1;
                }
            });
        }
    });

    var outArr = [];
    _.each(outGroupsCounts, function(val, key){
        if(val>1) outArr.push({
            group: +key,
            count: val,
            nick: groups[+key][0]
        });
    });

    outArr = _.sortBy(outArr, 'count').reverse();
    //console.log("outArr", outArr);

    //кто обращался ко мне
    // _.each(data, function (item) {
    //     if(!item[2]) return false;
    //     var gr = item[2].split('|');
    //
    //     if(gr.indexOf(sourceGroup) > -1){
    //         var n = item[0];
    //         if(inGroupsCounts[n]) {
    //             inGroupsCounts[n]++;
    //         } else {
    //             inGroupsCounts[n] = 1;
    //         }
    //     }
    //
    // });

    // var inArr = [];
    // _.each(inGroupsCounts, function(val, key){
    //     inArr.push({
    //         login: key,
    //         count: val,
    //         //nick: groups[+key][0]
    //     });
    // });
    // inArr = _.sortBy(inArr, 'count').reverse();
    //
    // console.log("inGroupsCounts", inArr);

    if(_.size(outArr) > 0) {
        console.log('add', login);
        relations[login] = outArr;
    }

}

function createFiles() {

    Users.find({mess_count: {$gt: 100}}, {}).toArray(function(err, users){

        _.each(users, function (user) {

            search(user.login, user.nick);

        });

        fs.writeFileSync('./stat/data/relations.json', JSON.stringify(relations));

    });

}

createFiles();


