
var mongo = require('mongoskin');
var socketIo = require('socket.io');
var _ = require('underscore');
var restify = require('restify');
var cors = require('cors');

var config = require('./configs/config.js');
var env = require('./include/env.js');
var router = require('./include/router.js');
var rooms = require('./include/rooms.js');
var plugins = require('./include/plugins.js');
var tools = require('./include/tools.js');
var fileserv = require('./include/fileserv.js');
var sessions = require('./include/sessions.js');

global.time = function(){return parseInt(new Date().getTime()/1000)};
global.isset = function(vr){return typeof(vr)!=='undefined'};
global.obj2arr = function(ob){var ar=[]; for(var i in ob) ar.push(i); return ar};

global.startTime = time();
global.anonims = {};
global.allUsers = {};
global.login2nick = {};

//config.mongoUser;
//config.mongoPass;

global.db = mongo.db('mongodb://' + config.mongoUser + ':' + config.mongoPass + "@" + config.mongoServer + ':' + config.mongoPort + '/' + config.mongoBaseName);
global.dbusers = db.bind('users');
global.dbmess = db.bind('messages');
global.dbrooms = db.bind('rooms');
global.dbhist = db.bind('history');
global.dbhistEv = db.bind('historyEvents');
global.dbplugins = db.bind('plugins');
global.dbsessions = db.bind('sessions');

dbrooms.update({}, {$set: {'userscount':0, 'users':{}}}, {'multi':true}, function(err,res){});

dbusers.find({}).toArray(function(e,r){
    if(!r)
        return false;
    for(var i=0; i < r.length; i++){
		login2nick[r[i].login] = r[i].nick;
	}
});

dbusers.find({voiceoff: true}).toArray(function(err, users){
    _.each(users, function(user){
        env.voiceOff[user.login] = true;
    });
});

sessions.clearOnline();

var server = restify.createServer({name: 'hitagi_io'});
var app = server.listen(config['httpPort'], function(){
    console.log('Hitagi Server Start');
    console.log('Listen port:', config['httpPort']);
});

global.io = socketIo(app);

server.use(cors());
server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.post('/upload/image', fileserv.uploadImage);
server.post('/upload/avatar/:user', fileserv.uploadAvatar);
server.get('/templates.handler.js', fileserv.templatesHandler);
server.get(/\/?.*/, restify.serveStatic({
    directory: tools.projectPath() + '/public',
    default: 'index.html',
    maxAge: 3600 // 1 hour
}));

io.on('connection', function(socket){

    return false;

    socket.isLogin = false;
    socket.profile = {};
    socket.client_ip = socket.handshake.headers["x-real-ip"];
    socket.hasRoom = function(room){
        var rooms = _.rest(_.keys(socket.rooms));
        return _.indexOf(rooms, room) > -1;
    };
    socket.roomsList = function(){
        var arr = _.rest(_.keys(socket.rooms));
        return _.filter(arr, function(item){
            return item.indexOf('pm_') == -1;
        });
    };
    socket.saveRooms = function(){
        socket.savedRooms =  _.rest(_.keys(socket.rooms));
    };

    router.rout(socket);

    socket.on('disconnect', function(){
        console.log('Socket disconnect');
        rooms.leaveAllRooms(socket, true);
        socket.session && sessions.updateOffline(socket.session._id);
    });

    console.log("Socket connect:", socket.client_ip);

});

io.socketsInRoom = function(room){
    var rooms = io.nsps['/'].adapter.rooms[room];
    if(!rooms)
        return [];
    var socketsKeys = _.keys(rooms.sockets);
    return _.map(socketsKeys, function(key){
        return io.sockets.connected[key];
    });
};

io.usersCountInRoom = function(room, user){
    var count = 0;
    _.each(io.socketsInRoom(room), function(s){
        if(!s.profile)
            return;
        if(s.profile.login == user)
            count++;
    });
    return count;
};

plugins.load();

