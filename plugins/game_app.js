var gifts = require('./game_items.js');
var quests = require('./game_quests.js');

var gameSave;

exports.load = function(){

	dbplugins.findOne({'bot':'Game'}, function(err, res){
		gameSave = res.data;
	});

}


exports.action = function(f, rec){
	if(rec.isBot) return rec;
	var com = f[1].toLowerCase();
	var mess = trim(f[2], ' ');
	var params = mess.split(' ');
	
	// данные юзера, который послал команду
	var userNick = rec.nick;
	var userName = rec.s.profile.login;
	var userSave = gameSave[userName] ? gameSave[userName] : {};
	var privateMode = userSave.privat ? false : true;

	/* общая информация об игре */
	if(com == 'игра'){ 

		rec.text = '<b>Игра 0.1b</b> <br> Чтобы начать новую игру ввердите <b class="bott">!новая_игра</b>';
		
		rec.toAll = privateMode;
		rec.save = false; 
		rec.isBot = true;
	}	
	if(com == 'приватная_игра'){ 
		if(mess == 'да'){
			gameSave[userName].privat = true;
			rec.text = 'Вы перешли в режим приватной игры';
		} else if(mess == 'нет'){
			gameSave[userName].privat = false;
			rec.text = 'Вы перешли в режим публичной игры';
		} else {
			var rejim = gameSave[userName].privat ? 'приватный' : 'публичный';
			rec.text = 'Текущий режим игры - '+rejim+'. Чтобы изменить режим игры, используйте команду: <b class="bott">!приватная_игра да|нет</b>';
		}
		updateStorage();
		rec.toAll = privateMode;
		rec.save = false; 
		rec.isBot = true;		
	}
	if(com == 'новая_игра'){ 

		var newGameText = 'Новая игра начата! Вам нужно распределить 10 баллов по навыкам **** через команду !мои_навыки';
		var userData = {
				nick: userNick,
				status: 'new',
				privat: false,
				start: time()
			}

		if(mess == 'да'){
			gameSave[userName] = userData;
			rec.text = newGameText;
		} else {
			if(gameSave[userName]){
				rec.text = 'Вы действительно хотите стиреть все данные и начать игру заново? Тогда введите команду <b>!новая_игра да</b>';
			} else {
				gameSave[userName] = userData;
				rec.text = newGameText;
			}
		}
	
		updateStorage();
		rec.toAll = privateMode;
		rec.save = false; 
		rec.isBot = true;
	}
	if(com == 'мои_навыки'){ 
		if(userSave.status == 'new'){
			var sum = parseInt(params[0]) + parseInt(params[1]) + parseInt(params[2]) + parseInt(params[3]) + parseInt(params[4]) + parseInt(params[5]);
			if(sum == 10){
				gameSave[userName].skills = {
					'mind': parseInt(params[0]),
					'strong': parseInt(params[1]),
					'agelity': parseInt(params[2]),
					'concentr': parseInt(params[3]),
					'cunning': parseInt(params[4]),
					'communit': parseInt(params[5])
				};
				gameSave[userName].status = 'play';
				rec.text = 'Ваши навыки успешно установлены! *показываем их* теперь выможте играть. Подсказать что делать дальше и какие действия доступны вам поможет команда <b>!что_делать</b>';
				updateStorage();
			} else {
				rec.text = 'Вы НЕ правильно ввели распределение навыков, попробуйте снова';
			}
		} else {
			rec.text = 'Вы не можете распередить навыки, начните игру заново чтобы это сделать (!новая_игра)';
		}
	
		rec.toAll = privateMode;
		rec.save = false; 
		rec.isBot = true;
	}		
	
	
	return rec;
}

/* tools */
function updateStorage(){
	dbplugins.update({'bot':'Game'}, {$set: {'data':gameSave}});
}
function trim(b,a){return ltrim(rtrim(b,a),a)}function ltrim(b,a){return b.replace(RegExp("^["+(a||"\\s")+"]+","g"),"")}function rtrim(b,a){return b.replace(RegExp("["+(a||"\\s")+"]+$","g"),"")};