
function initBot(includes) {
    var info = {
        name: 'Testovich',
        version: '0.1'
    };
    var helpFile = '';
    var parser = new includes.xml.Parser();
    var weatherCountres;

    function reciveMess(s, mes) {
        info.onRecive(s, mes);
    }

    includes.fs.readFile(config['serverDir'] + '/plugins/testbot_help.html', function (err, data) {
        helpFile = String(data);
    });
    includes.fs.readFile(config['serverDir'] + '/plugins/cities.xml', function (err, data) {
        parser.parseString(data, function (err, result) {
            weatherCountres = result.cities.country;
        });
    });

    info.send = function (rec) {
        var m = rec.text;
        rec.save = true;
        rec.toall = true;
        rec.proc = false;

        var rg = /^!([а-яА-Я]+) (.*)/gi;
        var found = rg.exec(m + ' ');
        if (found) {
            var com = found[1];
            var mess = trim(found[2], ' ');
            var param = mess.split(' ');


            if (com == 'я') {
                m = '<b>' + rec.nick + '</b> ' + mess;
                rec.toall = true;
            } else if (com == 'справка') {
                m = helpFile;
                rec.toall = false;
            } else if (com == 'нг') {
                m = 'До Нового Года осталось: ' + ng();
                rec.toall = false;
            } else if (com == 'песня') {
                m = rec.nick + ' предлагает послушать <a target="_blank" href="http://vk.com/audio?q=' + encodeURIComponent(mess) + '">' + mess + '</a>';
                if (mess == '') m = 'Введите название песни, например: !песня страна лимония';
                rec.toall = true;
            } else if (com == 'погода') {
                if (mess == '')
                    m = 'Введите город, например: !погода москва';
                else {
                    mess = mess.toLowerCase();
                    m = 'Город "' + mess + '" не найден. Убедитесь в правильности написания города';
                    for (var stran in weatherCountres) {
                        for (var gor in weatherCountres[stran].city) {
                            var city = weatherCountres[stran].city[gor];
                            if (city['_'].toLowerCase() == mess) {
                                getWeather(rec, {
                                    name: city['_'],
                                    region: city.$.part,
                                    country: city.$.country,
                                    id: city.$.id
                                });
                                m = 'Получение прогноза погоды...';
                            };
                        }
                    }

                }
                rec.toall = false;
            } else if (com == 'кубик') {
                if (Math.random() < 0.95) {
                    m = '<b>' + rec.nick + '</b> бросил кубик, результат: ' + (Math.floor(Math.random() * 6) + 1);
                } else {
                    var choice = Math.random();
                    if (choice <= 0.25) {
                        m = rec.nick + ', у тебя НОЛЬ, неудачник!';
                    } else if (choice <= 0.50) {
                        m = rec.nick + ', ты выйграл ПРИЗ! <img src="http://s3.zerochan.net/Yokune.Ruko.240.267453.jpg"/>';
                    } else if (choice <= 0.75) {
                        m = rec.nick + ', сделай ЭТО нежно!!! <img src="http://i4.imageban.ru/thumbs/2011.01.04/ea60e305f4b70adfdbb33cefc10eaa77.png"/>';
                    } else {
                        m = rec.nick + ', позаботься обо мне!!! <img src="http://worldschooluniform.ru/_ph/24/1/846171072.jpg"/>';
                    }
                    // сюда больше, БОЛЬШЕ пасхальных яиц :)
                }
                rec.toall = true;

            } else if (com == 'монета') {
                if (Math.random() > 0.5) {
                    m = '<b>' + rec.nick + '</b> бросил монету, выпал ОРЕЛ';
                } else {
                    m = '<b>' + rec.nick + '</b> бросил монету, выпала РЕШКА';
                }
                rec.toall = true;
            } else if (com == 'бутылка') {
                var target = 'NONE';
                console.log(rec);

                // получаем список юзеров с никами

                // удаляем из списка ников ник того, кто отправил запрос

                // среди оставшихся ников выбираем один случайно (target)

                m = '<b>' + rec.nick + '</b> крутанул бутылочку, она повернулась на <b>' + target + '</b>';

                rec.toall = true;
            } else {
                m = 'Неизвестная команда "' + com + '"';
                rec.toall = false;
            }

            rec.proc = true;
            rec.save = false;
        }

        rec.text = m;

        return rec;
    }


    function getWeather(rec, city) {
        var p = {};
        getPage('http://export.yandex.ru/weather-ng/forecasts/' + city.id + '.xml', function (txt) {

            parser.parseString(txt, function (err, result) {
                var fact = result.forecast.day[0];
                var tom = result.forecast.day[1];

                p.fact = {
                    date: fact.$.date,
                    sunrise: fact.sunrise[0],
                    sunset: fact.sunset[0],
                    day: {
                        temp: fact.day_part[4].temperature[0],
                        type: fact.day_part[4].weather_type[0],
                        wind: fact.day_part[4].wind_speed[0]
                    },
                    night: {
                        temp: fact.day_part[5].temperature[0],
                        type: fact.day_part[5].weather_type[0],
                        wind: fact.day_part[5].wind_speed[0]
                    }
                };
                p.tomorrow = {
                    date: tom.$.date,
                    sunrise: tom.sunrise[0],
                    sunset: tom.sunset[0],
                    day: {
                        temp: tom.day_part[4].temperature[0],
                        type: tom.day_part[4].weather_type[0],
                        wind: tom.day_part[4].wind_speed[0]
                    },
                    night: {
                        temp: tom.day_part[5].temperature[0],
                        type: tom.day_part[5].weather_type[0],
                        wind: tom.day_part[5].wind_speed[0]
                    }
                };


                var prognoz = '<b>Прогноз погоды:</b><br />' + city.country + ', ' + city.region + ', ' + city.name + '<br /><br /><b>Сегодня ' + p.fact.date + ':</b><br />Днем ' + p.fact.day.temp + ' С, ' + p.fact.day.type + ', скорость ветра ' + p.fact.day.wind + ' м/с<br />Ночью ' + p.fact.night.temp + ' С, ' + p.fact.night.type + ', скорость ветра ' + p.fact.night.wind + ' м/с.<br />Восход в ' + p.fact.sunrise + ', заход в ' + p.fact.sunset + '<br /><br /><b>Завтра ' + p.tomorrow.date + ':</b><br />Днем ' + p.tomorrow.day.temp + ' С, ' + p.tomorrow.day.type + ', скорость ветра ' + p.tomorrow.day.wind + ' м/с<br />Ночью ' + p.tomorrow.night.temp + ' С, ' + p.tomorrow.night.type + ', скорость ветра ' + p.tomorrow.night.wind + ' м/с.<br />Восход в ' + p.tomorrow.sunrise + ', заход в ' + p.tomorrow.sunset;
                reciveMess(rec.socket, prognoz);

            });

        });

    }

    function getPage(pageurl, callback) {
        var options = {
            host: includes.url.parse(pageurl).host,
            port: 80,
            path: includes.url.parse(pageurl).pathname
        };
        var text = '';
        includes.http.get(options, function (res) {
            res.on('data', function (data) {
                text += data;
            }).on('end', function () {
                callback(text);
            });
        });
    };


    return info;

}


function stripBB(str) {
    return str.replace(/\[[^\]]+\]/g, '')
}

function ng() {
    now = new Date;
    ex = new Date(2014, 0, 1, 0, 0, 0);
    hours = now.getHours();
    minutes = now.getMinutes();
    seconds = now.getSeconds();
    timeStr = "" + hours;
    timeStr += (10 > minutes ? ":0" : ":") + minutes;
    timeStr += (10 > seconds ? ":0" : ":") + seconds;
    date = now.getDate();
    month = now.getMonth() + 1;
    year = now.getYear();
    dateStr = "" + date;
    dateStr += (10 > month ? "/0" : "/") + month;
    dateStr += "/" + year;
    ostStr = "";
    x = (ex.getTime() - now.getTime()) / 1E3;
    ostStr = Math.floor(x / 60 / 60) + " \u0447. ";
    ostStr = ostStr + Math.floor(60 * (x / 60 / 60 - Math.floor(x / 60 / 60))) + " \u043c\u0438\u043d. ";
    x = 60 * (60 * (x / 60 / 60 - Math.floor(x / 60 / 60)) - Math.floor(60 * (x / 60 / 60 - Math.floor(x / 60 / 60))));
    ostStr = ostStr + Math.floor(x) + " \u0441\u0435\u043a. ";
    x = 1E3 * (x - Math.floor(x));
    return ostStr
};

function trim(b, a) {
    return ltrim(rtrim(b, a), a)
}
function ltrim(b, a) {
    return b.replace(RegExp("^[" + (a || "\\s") + "]+", "g"), "")
}
function rtrim(b, a) {
    return b.replace(RegExp("[" + (a || "\\s") + "]+$", "g"), "")
};