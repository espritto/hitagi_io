var im = require('imagemagick');
var _ = require('underscore');
var fs = require("fs");
var url = require("url");
var config = require('../configs/config.js');
var api = require('./api.js');
var tools = require('./tools.js');
var htmlmin = require('htmlmin');

var noavaname = 'noavatar.gif';
var cachecontr = 'max-age=2592000, must-revalidate';
var servname = 'Hitagi Chat Server';

var templatesHandlerCache = '';

module.exports.uploadImage = function (req, res) {

    var data = req.body;

    var n = data.indexOf('/') + 1;
    var ext = data.substring(n, data.indexOf(';'));
    var bmp = new Buffer(data.substring(data.indexOf(',') + 1), 'base64');

    var nid = require('crypto').randomBytes(16).toString('hex');

    var orig_name = tools.projectPath() + '/public/share_images/orig/' + nid + '.' + ext;
    var optim_name = tools.projectPath() + '/public/share_images/optim/' + nid + '.' + ext;

    fs.writeFile(orig_name, bmp, function (err) {

        if (err)
            return res.send({status: 'error', 'reason': 'upload'});

        im.identify([orig_name], function (err, output) {

            console.log("uploadImage identify error", err);

            if (err)
                return res.send({status: 'error', 'reason': 'read'});

            var fldat = output.split(' ');
            var realext = fldat[1];
            var sizes = fldat[2].split('x');
            var sizeX = sizes[0] * 1, sizeY = sizes[1] * 1;

            if (realext != 'GIF' && (sizeX > 600 || sizeY > 600)) {
                im.resize({
                    srcPath: orig_name,
                    dstPath: optim_name,
                    width: 600,
                    height: 600
                }, function (err, stdout, stderr) {

                    return res.send({
                        status: 'ok',
                        urlThumb: '/share_images/optim/' + nid + '.' + ext,
                        urlImage: '/share_images/orig/' + nid + '.' + ext
                    });

                });
            } else {
                return res.send({
                    status: 'ok',
                    urlThumb: '/share_images/orig/' + nid + '.' + ext,
                    urlImage: '/share_images/orig/' + nid + '.' + ext
                });
            }

        });

    });

};

module.exports.uploadAvatar = function (req, res) {

    var socket = io.sockets.connected[req.params['user']];

    if (!socket)
        return res.send({status: 'error', reason: 'socket not found'});

    var user = socket.profile.login;

    var data = req.body;
    var n = data.indexOf('/') + 1;
    var ext = data.substring(n, data.indexOf(';'));
    var bmp = new Buffer(data.substring(data.indexOf(',') + 1), 'base64');
    var tmp_name = tools.projectPath() + '/public/avatars/tmp/' + user + '.' + ext;
    var dest_name = tools.projectPath() + '/public/avatars/' + user + '.jpg';
    var addGif = (ext == 'gif') ? '[0]' : '';

    fs.writeFile(tmp_name, bmp, (err) => {

        if (err)
            return res.send({status: 'error', reason: 'uploading error'});

        im.crop({
            srcPath: tmp_name + addGif,
            dstPath: dest_name,
            width: 80,
            height: 80,
            quality: 0.96
        }, function (err, stdout, stderr) {

            if (err)
                return res.send({status: 'error', reason: 'cropping error'});

            fs.unlink(tmp_name);

            dbusers.update({'login': user}, {$inc: {'ava_index': 1}}, function () {
            });
            dbhistEv.insert({'user': user, 'event': 5, 'date': time()}, function (err, res) {
            });
            socket.profile.avaindex++;

            var avaUrl = '/avatars/' + user + '.jpg?' + socket.profile.avaindex;

            // надо отправить сообщения во все комнаты пользователя
            _.each(socket.roomsList(), function (room) {
                io.to(room).emit('setavatar', {'status': 'ok', 'user': user, 'url': avaUrl});
            });

            res.send({status: 'ok', url: avaUrl});

        });

    });

};

module.exports.templatesHandler = function (req, res) {

    var needCache = req.query.cache != 'false';
    res.setHeader('content-type', 'application/javascript');

    if(!templatesHandlerCache || !needCache) {

        var path = tools.projectPath() + '/public/templates';
        var data = {};
        fs.readdir(path, function(err, files) {
            files.forEach(function(name) {
                //todo тут потенциальная уязвимость, нужно использовать асинхронные функции чтения файла
                var content = fs.readFileSync(path + '/' + name).toString();
                data[name.replace(/\.[^/.]+$/, "")] = htmlmin(content);
            });

            templatesHandlerCache = 'var templates = ' + JSON.stringify(data) + ';';
            res.end(templatesHandlerCache);

        });

    } else {
        res.end(templatesHandlerCache);
    }

};
