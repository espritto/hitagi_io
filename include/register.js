function registerUser(param, s){
	var login = pWrap(param['login']);
	var nick = pWrap(param['nickname']);	
	var pass = pWrap(param['pass']);

	if(s.isLogin && s.profile['privilege'] != 0){
		sendError('alreadyauth', s);
		return false;
	}	

	for(var i=0; i<config['busyLogins'].length-1; i++){
		if(config['busyLogins'][i] == login || config['busyLogins'][i] == nick){
			sendError('busynicklogin', s);
			return false;
		}
	}

	if(pass.length != 32) {
		sendError('wrongpass', s);
		return false;
	}
	
	if(!/^[\w-]{3,15}$/i.test(login)){
		sendError('wronglogin', s);
		return false;		
	}
	
	if(nick.length < 3 || nick.length > config['maxNickLength']){
		sendError('wrongnick', s);
		return false;				
	}
	
	dbusers.findOne({'login':login}, function(err, result){
		if(!result){
			dbusers.findOne({'nick':nick}, function(err, result){
				if(!result){

					var newuser = {
						'login':login,
						'nick':nick,
						'pass':pass,
						'ip':s.client_ip,
						'reg_date': time(),
						'statustext':'',
						'state':0,
						'privilege':2,
						'gender':0,
						'block_reason':'',
						'last_login':0,
						'ava_index':0,
						'vk_id':0,
						'client':0,
						'rating':0,
						'mess_count': 0,
						'textcolor':'#000',
						'block':0,
						'profile_visible':1
					};
					
					dbusers.insert(newuser, function(err, result){
						sendOk(s);
					});						
				} else {
					sendError('busynick', s);
				}
			});
		} else {
			sendError('busylogin', s);
		}
	});

}

function adminReg(param, s){
	var login = pWrap(param['login']);
	var nick = pWrap(param['nickname']);	
	var pass = pWrap(param['pass']);

	if(s.isLogin){
		s.json_send({'type':'adminreg', 'status':'error', 'reason':'notlogin'});
		return false;
	}	
	
	if(s.profile.privilege >= 2){
		s.json_send({'type':'adminreg', 'status':'error', 'reason':'notallowed'});
		return false;
	}	

	for(var i=0; i<config['busyLogins'].length-1; i++){
		if(config['busyLogins'][i] == login || config['busyLogins'][i] == nick){
			s.json_send({'type':'adminreg', 'status':'error', 'reason':'busynicklogin'});
			return false;
		}
	}

	if(pass.length != 40) {
		s.json_send({'type':'adminreg', 'status':'error', 'reason':'wrongpass'});
		return false;
	}
	
	if(!/^[\w-]{3,15}$/i.test(login)){
		s.json_send({'type':'adminreg', 'status':'error', 'reason':'wronglogin'});
		return false;		
	}
	
	if(nick.length < 3 || nick.length > config['maxNickLength']){
		s.json_send({'type':'adminreg', 'status':'error', 'reason':'wrongnick'});
		return false;				
	}

	dbusers.findOne({'login':login}, function(err, result){
		if(!result){
			dbusers.findOne({'nick':nick}, function(err, result){
				if(!result){
				
					/* ���������� ������ ����� � ���� */
					var newuser = {
						'login':login,
						'nick':nick,
						'pass':pass,
						'ip':'0.0.0.0',
						'reg_date': time(),
						'statustext':'',
						'state':0,
						'privilege':2,
						'gender':0,
						'block_reason':'',
						'last_login':0,
						'ava_index':0,
						'vk_id':0,
						'client':0,
						'rating':0,
						'block':0,
						'profile_visible':1
					};
					
					dbusers.insert(newuser, function(err, result){});	
					s.json_send({'type':'adminreg', 'status':'ok', 'login':login, 'nick':nick});

				} else {
					s.json_send({'type':'adminreg', 'status':'error', 'reason':'busynick'});
				}
			});
		} else {
			s.json_send({'type':'adminreg', 'status':'error', 'reason':'busylogin'});
		}
	});

}

function sendError(reason, s){
	s.json_send({'type':'register', 'status':'error', 'reason':reason});
}
function sendOk(s){
	s.json_send({'type':'register', 'status':'ok'});
}
function pWrap(par){
	return isset(par) ? par : '';
}

exports.registerUser = registerUser;
exports.adminReg = adminReg;