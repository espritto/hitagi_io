
var tools = require('./tools.js');
var env = require('./env.js');

function globPriv(param) {
    var socket = this;
    var priv = pWrap(param['priv']) * 1;
    var puser = pWrap(param['user']);
    var user = socket.profile.login;
    var myPriv = socket.profile.privilege;

    if (!socket.isLogin) {
        socket.emit('globprivilege', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }
    if (priv != 1 && priv != 2 && priv != 4) {
        socket.emit('globprivilege', {'status': 'error', 'reason': 'badpriv'});
        return false;
    }

    dbusers.findOne({'login': puser}, function (err, res) {
        if (res) {
            var upriv = res['privilege'];
            if (upriv == 0) {
                socket.emit('globprivilege', {'status': 'error', 'reason': 'notallowed'});
                return false;
            }
            if (upriv == 3) {
                socket.emit('globprivilege', {'status': 'error', 'reason': 'notallowed'});
                return false;
            }
            if (upriv == 1 && myPriv == 1) {
                socket.emit('globprivilege', {'status': 'error', 'reason': 'notallowed'});
                return false;
            }

            dbusers.updateById(res['_id'], {$set: {'privilege': priv}}, function (err, result) {
            });

            io.broadcast.emit('globprivilege', {'status': 'ok', 'user': puser, 'priv': priv});

        } else {
            socket.emit('globprivilege', {'status': 'error', 'reason': 'notfound'});
        }
    });

}

function roomPriv(param) {
    var socket = this;
    var priv = pWrap(param['priv']) * 1;
    var puser = pWrap(param['user']);
    var room = pWrap(param['room']);
    var user = socket.profile.login;
    var myPriv = socket.profile.privilege;

    if (!socket.isLogin) {
        socket.emit('roomprivilege', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }
    if (priv != 0 || priv != 2) {
        socket.emit('roomprivilege', {'status': 'error', 'reason': 'badpriv'});
        return false;
    }

    dbrooms.findOne({'name': room}, function (err, res) {
        if (res) {
            if (res.owner != user || myPriv != 0 || myPriv != 1) {
                socket.emit('roomprivilege', {'status': 'error', 'reason': 'notallowed'});
                return false;
            }
            if (priv == 0) {
                dbrooms.update({'name': room}, {$pull: {'moderators': puser}}, function (err, resu) {
                });
            }
            if (priv == 2) {
                dbrooms.update({'name': room}, {$addToSet: {'moderators': puser}}, function (err, resu) {
                });
            }

            io.to(room).emit('roomprivilege', {'status': 'ok', 'user': puser, 'priv': priv});

        } else {
            socket.emit('roomprivilege', {'status': 'error', 'reason': 'noroom'});
        }
    });

}

function banOn(param) {
    var socket = this;
    var user = pWrap(param['user']);
    var room = pWrap(param['room']);
    var reason = pWrap(param['reason']);
    var btime = pWrap(param['time']) * 1;
    var me = socket.profile.login;
    var myPriv = socket.profile.privilege;

    if (!socket.isLogin) {
        socket.emit('banon', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    dbrooms.findOne({'name': room}, function (err, res) {
        if (res) {
            getUserPrivs(me, room, function (meprev) {

                if (meprev.global <= 2 && (meprev.room == 1 || meprev.room == 2)) {
                    getUserPrivs(user, room, function (usprev) {
                        if (usprev.global <= 1 || usprev.room == 1) {
                            socket.emit('banon', {'status': 'error', 'reason': 'notallow'});
                            return false;
                        }

                        var rusers = res.users;
                        var ba = res.banned;
                        ba[user] = {
                            'reason': reason,
                            'by': me,
                            'expires': time() + (btime * 60),
                            'total': btime,
                            'start': time()
                        };

                        io.to(room).emit('banon', {
                            'status': 'ok',
                            'user': user,
                            'reason': reason,
                            'time': btime,
                            'room': room
                        });

                        delete rusers[user];
                        //delete allRooms[room][user];
                        dbrooms.updateById(res['_id'], {$set: {'users': rusers, 'banned': ba}}, function (err, result) {
                        });
                    });
                } else {
                    socket.emit('banon', {'status': 'error', 'reason': 'notallow'});
                    return false;
                }
            });
        } else {
            socket.emit('banon', {'status': 'error', 'reason': 'noroom'});
        }
    });

}


function internalBan(room, user, btime, reason) {

    dbrooms.findOne({'name': room}, function (err, res) {
        if (res) {

            var rusers = res.users;
            var ba = res.banned;
            ba[user] = {
                'reason': reason,
                'by': 'admin',
                'expires': time() + (btime * 60),
                'total': btime,
                'start': time()
            };

            io.to(room).emit('banon', {'status': 'ok', 'user': user, 'reason': reason, 'time': btime, 'room': room});

            delete rusers[user];
            //delete allRooms[room][user];
            dbrooms.updateById(res['_id'], {$set: {'users': rusers, 'banned': ba}}, function (err, result) {
            });

        }
    });

}


function banOff(param) {

}

function voiceOff(param) {
    var socket = this;
    var duration = pWrap(param['duration']);
    var reason = pWrap(param['reason']);
    var user = pWrap(param['user']);
    var room = pWrap(param['room']);
    var me = socket.profile.login;
    var myPriv = socket.profile.privilege;

    if (!socket.isLogin) {
        socket.emit('voiceoff', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    dbusers.findOne({login: user}, function (err, res) {
        if(!res)
            return socket.emit('voiceoff', {'status': 'error', 'reason': 'no_user'});

        dbusers.updateById(res['_id'], {$set: {voiceoff: true}}, function (err, result) {
            io.to(room).emit('voiceoff', {'status': 'ok', 'user': user, reason: reason, room: room, time: duration});
        });

        env.voiceOff[user] = true;

    });

}

function voiceOn(param) {

    var socket = this;
    var user = pWrap(param['user']);
    var room = pWrap(param['room']);
    var me = socket.profile.login;
    var myPriv = socket.profile.privilege;

    if (!socket.isLogin) {
        socket.emit('voiceon', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    dbusers.findOne({login: user}, function (err, res) {
        if(!res)
            return socket.emit('voiceon', {'status': 'error', 'reason': 'no_user'});

        dbusers.updateById(res['_id'], {$set: {voiceoff: false}}, function (err, result) {
            io.to(room).emit('voiceon', {'status': 'ok', 'user': user, room: room});
        });

        env.voiceOff[user] = false;

    });

}

function kick(param) {
    var socket = this;
    var user = pWrap(param['user']);
    var room = pWrap(param['room']);
    var me = s.profile.login;
    var myPriv = socket.profile.privilege;

    if (!socket.isLogin) {
        socket.emit('kick', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    dbrooms.findOne({'name': room}, function (err, res) {
        if (res) {
            getUserPrivs(me, room, function (meprev) {

                if (meprev.global <= 2 && (meprev.room == 1 || meprev.room == 2)) {
                    getUserPrivs(user, room, function (usprev) {
                        if (usprev.global <= 1 || usprev.room == 1) {
                            socket.emit('kick', {'status': 'error', 'reason': 'notallow'});
                            return false;
                        }

                        var rusers = res.users;

                        io.to(room).emit('kick', {'status': 'ok', 'user': user, 'room': room});

                        delete rusers[user];
                        //delete allRooms[room][user];
                        dbrooms.updateById(res['_id'], {$set: {'users': rusers}}, function (err, result) {
                        });
                    });
                } else {
                    socket.emit('kick', {'status': 'error', 'reason': 'notallow'});
                    return false;
                }
            });
        } else {
            socket.emit('kick', {'status': 'error', 'reason': 'noroom'});
        }
    });


}

function internalKick(room, user) {
    dbrooms.findOne({'name': room}, function (err, res) {
        if (res) {
            var rusers = res.users;

            io.to(room).emit('kick', {'status': 'ok', 'user': user, 'room': room});
            delete rusers[user];
            //delete allRooms[room][user];
            dbrooms.updateById(res['_id'], {$set: {'users': rusers}}, function (err, result) {
            });
        }
    });
}


function getUserPrivs(login, room, callback) {
    var rezul = {};
    dbusers.findOne({'login': login}, function (err, res) {
        if (res) {
            var upriv = res['privilege'];
            if (room != '') dbrooms.findOne({'name': room}, function (err, res2) {
                if (res2) {
                    var us = res2.users[login];
                    var roomPriv = 0;

                    if (res2['owner'] == login) roomPriv = 1;
                    if (isset(res2.moderators[login])) roomPriv = 2;
                    if (isset(res2.novoiced[login])) roomPriv = 3;
                    if (isset(res2.banned[login])) roomPriv = 4;

                    if (isset(us)) {
                        rezul.global = upriv;
                        rezul.icon = us.priv;
                        rezul.room = roomPriv;
                        callback(rezul);
                    } else {
                        callback('nouserinroom');
                    }
                } else {
                    callback('noroom');
                }
            }); else {
                rezul.global = upriv;
                callback(rezul);
            }
        } else {
            callback('nouser');
        }
    });
}

function pWrap(par) {
    return isset(par) ? par : '';
}

exports.globpriv = globPriv;
exports.roompriv = roomPriv;
exports.banon = banOn;
exports.banoff = banOff;
exports.voiceon = voiceOn;
exports.voiceoff = voiceOff;
exports.kick = kick;
exports.internalKick = internalKick;
exports.internalBan = internalBan;