var config = require('../configs/config.js');
var tools = require('./tools.js');
var plugins = require('./plugins.js');
var ObjectID = require('mongodb').ObjectID;
var sessions = require('./sessions.js');
var _ = require('underscore');
var env = require('./env.js');

function createRoom(param) {
    var socket = this;
    var name = pWrap(param['name']);
    var capt = pWrap(param['caption']);
    var topic = pWrap(param['topic']);
    var hidden = param['hidden'] ? 1 : 0;

    if (!socket.isLogin) {
        socket.emit('createroom', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }
    if (name == '') {
        socket.emit('createroom', {'status': 'error', 'reason': 'noname'});
        return false;
    }
    if (capt == '') {
        socket.emit('createroom', {'status': 'error', 'reason': 'nocapt'});
        return false;
    }

    dbrooms.findOne({'name': name}, function (err, res) {
        if (!res) {
            var room = {
                'name': name,
                'caption': capt,
                'createdate': time(),
                'owner': socket.profile.login,
                'hidden': hidden,
                'banned': {},
                'novoiced': {},
                'moderators': {},
                'users': {},
                'totalmessages': 0,
                'userscount': 0,
                'topic': topic
            };
            dbrooms.insert(room, function (err, res) {
            });
            socket.emit('createroom', {'status': 'ok', 'name': name});
        } else {
            socket.emit('createroom', {'status': 'error', 'reason': 'busyname'});
        }
    });

}

function joinRoom(param) {
    var socket = this;
    var roomName = pWrap(param['room']);
    var count = pWrap(param['count']) || config.lastMessagesLimit;
    var userLogin = socket.profile.login;
    var userId = socket.profile._id;

    if (!socket.isLogin) {
        socket.emit('joinroom', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    dbrooms.findOne({'name': roomName}, function (err, room) {
        if (!room)
            return socket.emit('joinroom', {'status': 'error', 'reason': 'noroom'});

        var roomUsers = room.users;
        var base = [];
        var priva = 0;
        if (userLogin == room.owner) priva = 1;
        if (isset(room.moderators[userLogin])) priva = 2;
        if (isset(room.novoiced[userLogin])) priva = 3;
        if (isset(room.banned[userLogin])) {
            var exp = room.banned[userLogin].expires;
            if (time() >= exp) {
                delete room.banned[userLogin];
                dbrooms.updateById(room['_id'], {$set: {'banned': room.banned}}, function (err, result) {
                });
            } else {
                socket.emit('joinroom', {'status': 'banned', 'expires': exp - time()});
                return false;
            }
        }

        roomUsers[userLogin] = time();
        dbrooms.updateById(room['_id'], {$set: {'users': roomUsers, 'userscount': _.size(roomUsers)}}, function (err, result) {
        });

        //var someUsersCount = io.usersCountInRoom(roomName, userLogin);
        socket.join(roomName);

        var forSend = {
            login: userLogin,
            nick: socket.profile.nick,
            deviceType: tools.getDeviceByUA(socket.session.client),
            avaurl: socket.profile.avaurl,
            state: socket.profile.state,
            statustext: socket.profile.statustext,
            voiceoff: socket.profile.voiceoff,
            roomPriv: priva,
            globPriv: socket.profile.privilege,
            awayStatus: 0
        };

        dbhistEv.find({user: userLogin, room: roomName, event: 2}, {
            limit: 1,
            sort: {date: -1}
        }).toArray(function (err, lastDate) {
            if (lastDate.length > 0) {
                dbhist.find({
                    room: roomName,
                    date: {'$gt': lastDate[0].date}
                }, ['date']).toArray(function (err, lastMes) {
                    nextStep1(lastMes.length);
                });
            } else {
                nextStep1(0);
            }

        });

        function nextStep1(newMessages) {

            var usersList = _.map(io.socketsInRoom(roomName), function (s) {
                var profile = _.clone(s.profile);

                var priva2 = 0;
                if (profile.login == room.owner) priva2 = 1;

                if (isset(room.moderators[profile.login])) priva2 = 2;
                if (isset(room.novoiced[profile.login])) priva2 = 3;

                profile.roomPriv = priva2;
                profile.globPriv = profile.privilege;
                profile.deviceType = tools.getDeviceByUA(s.session.client);
                return profile;
            });


            // этот колбэк сработает только если у юзера одна сессия
            sessions.checkIsOneSession(userId, function(){

                io.to(roomName).emit('userjoined', {
                    room: roomName,
                    name: userLogin,
                    data: forSend
                });

            });

            dbhist.find({'room': roomName, 'text': {$exists: true}, 'deleted': {$exists: false}}, {
                limit: count,
                sort: {'date': -1}
            }).toArray(function (err2, res2) {

                _.each(res2, function (mes) {
                    var ms = {
                        'u': mes.user,
                        'd': mes.date,
                        'c': mes.cl,
                        'r': mes.room,
                        't': mes.text,
                        'id': mes._id,
                        'bot': mes.isbot
                    };
                    ms.n = login2nick[mes.user];
                    base.push(ms);
                });

                socket.emit('joinroom', {
                    'status': 'ok',
                    'name': roomName,
                    'caption': room.caption,
                    'newmes': newMessages,
                    'topic': room.topic,
                    'count': usersList.length,
                    'users': _.indexBy(usersList, 'login'),
                    'messages': base.reverse()
                });

            });


            setTimeout(function () {
                socket.saveRooms();
            }, 100);

            dbhistEv.insert({'room': roomName, 'user': userLogin, 'event': 1, 'date': time()}, function (err, res) {
            });

        }

    });

}
function leaveRoom(param) {
    var socket = this;
    var room = pWrap(param['room']);
    var minusCount = param.byDisconnect ? 1 : 0;

    if (!socket.isLogin) {
        socket.emit('leaveroom', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    //if(!socket.hasRoom(room)){
    //   socket.emit('leaveroom', {'status':'error', 'reason':'notinroom'});
    //	return false;
    //}

    var someUsersCount = io.usersCountInRoom(room, socket.profile.login);

    console.log("leave user", socket.profile.login, someUsersCount);

    if (someUsersCount > 1 - minusCount) {
        socket.emit('leaveroom', {'status': 'ok', 'room': room});
        setTimeout(function () {
            socket.saveRooms();
        }, 100);
        return socket.leave(room);
    }

    dbrooms.findOne({'name': room}, function (err, res) {
        if (!res)
            return socket.emit('leaveroom', {'status': 'error', 'reason': 'noroom'});

        var rusers = res.users;
        delete rusers[socket.profile.login];
        var pdata = {
            'userscount': _.size(rusers),
            'users': rusers
        };

        dbrooms.updateById(res['_id'], {$set: pdata}, function (err, result) {
        });

        socket.emit('leaveroom', {'status': 'ok', 'room': room});

        socket.leave(room);
        socket.to(room).emit('userleaved', {'name': socket.profile.login, 'room': room});

        dbhistEv.insert({'room': room, 'user': socket.profile.login, 'event': 2, 'date': time()}, function (err, res) {
        });

    });

}

function leaveAllRooms(socket, byDisconnect) {
    //console.log("leaveAllRooms", socket.savedRooms);

    _.each(socket.savedRooms, function (room) {
        if (room != 'pm_' + socket.profile.login)
            leaveRoom.call(socket, {room: room, byDisconnect: byDisconnect});
    });

}

function chat(param) {
    var socket = this;
    var room = pWrap(param['room']);
    var text = pWrap(param['text']);
    var color = pWrap(param['cl']);

    if (!socket.isLogin)
        return socket.emit('chat', {'status': 'error', 'reason': 'notlogin'});

    if (color == '')
        color = '000';

    if (!socket.hasRoom(room))
        return socket.emit('chat', {'status': 'error', 'reason': 'notinroom'});

    if(env.voiceOff[socket.profile.login])
        return socket.emit('chat', {'status': 'error', 'reason': 'voiceoff'});

    if (socket.profile.privilege >= 2)
        text = escapeHtml(text);

    dbrooms.findOne({'name': room}, function (err, res) {
        if (res) {
            var rusers = res.users;

            plugins.bot(socket, {text: text, room: room, users: rusers, nick: socket.profile.nick}, function(bt){

                text = bt.text;

                var insdat = {'room': room, 'user': socket.profile.login, 'text': text, 'cl': color, 'date': time()};
                if (bt.isBot) insdat.isbot = 1;

                if (bt.save) {
                    dbrooms.updateById(res['_id'], {$inc: {'totalmessages': 1}}, function (err, result) {
                    });
                    dbusers.update({'login': socket.profile.login}, {
                        $inc: {'mess_count': 1},
                        $set: {'textcolor': color}
                    }, function (err, result) {
                    });
                    dbhist.insert(insdat, savemess);
                } else {
                    savemess(true, {});
                }

                function savemess(err, res) {
                    var messag = {
                        'u': socket.profile.login,
                        'c': color,
                        't': text,
                        'id': insdat['_id'],
                        'r': room
                    };

                    if (bt.isBot) messag.isbot = 1;
                    if (bt.toAll) {
                        io.to(room).emit('chat', messag);
                    } else {
                        socket.emit('chat', messag);
                    }
                }

            });

        } else {
            socket.emit('chat', {'status': 'error', 'reason': 'noroom'});
        }
    });

}

function bot_chat(param) {
    var socket = this;
    var room = param['room'];
    var text = param['text'];
    var isSave = param['create'];
    var toAll = param['all'];

    dbrooms.findOne({'name': room}, function (err, res) {

        var rusers = res.users;

        var insdat = {
            'room': room,
            'user': socket.profile.login,
            'text': text,
            'cl': 'black',
            'date': time()
        };
        insdat.isbot = 1;

        if (isSave) {
            dbhist.insert(insdat, savemess);
        } else {
            savemess(true, {});
        }

        function savemess(err, res) {
            var messag = {
                'type': 'chat',
                'u': socket.profile.login,
                'c': 'black',
                't': text,
                'id': insdat['_id'],
                'r': room
            };
            messag.isbot = 1;
            if (toAll) {
                io.to(room).emit('chat', messag);
            } else {
                socket.emit('chat', messag);
            }
        }

    });

}

function chatcorrect(param) {
    var socket = this;
    var text = pWrap(param['text']);
    var user = socket.profile.login;

    if (!socket.isLogin) {
        socket.emit('chatcorrect', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    dbhist.findOne({'user': user}, {sort: {'date': -1}}, function (err, res) {
        dbhist.updateById(res['_id'], {$set: {'text': text}}, function (e, r) {
        });
        io.to(res.room).emit('chatcorrect', {'status': 'ok', 'newtext': text, 'mid': res['_id']});
    });

}

function setTopic(param) {
    var socket = this;
    var room = pWrap(param['room']);
    var topic = pWrap(param['topic']);

    dbrooms.findOne({'name': room}, function (err, res) {
        if (res) {

            if (res.owner == socket.profile.login || socket.profile.privilege < 2) {
                io.to(room).emit('settopic', {'status': 'ok', 'room': room, 'topic': topic});
                dbrooms.updateById(res['_id'], {$set: {'topic': topic}}, function (err, result) {
                });
            } else {
                socket.emit('settopic', {'status': 'error', 'reason': 'nopriv'});
            }

        } else {
            socket.emit('settopic', {'status': 'error', 'reason': 'noroom'});
        }
    });

}

function eraseMes(param) {
    var socket = this;
    var mid = pWrap(param['mid']);

    if (!socket.isLogin) {
        socket.emit('erasemessage', {'type': 'erasemessage', 'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    if (socket.profile.privilege > 1) {
        socket.emit('erasemessage', {'status': 'error', 'reason': 'noprivilege'});
        return false;
    }

    dbhist.updateById(mid, {$set: {'deleted': 1}}, function (err, result) {
    });
    dbhist.findOne({'_id': new ObjectID(mid)}, function (err, res) {
        if (res) {
            io.to(res.room).emit('erasemessage', {'status': 'ok', 'mid': mid});
            dbusers.update({'login': res.user}, {$inc: {'mess_count': -1, 'rating': -5}}, function (err, result) {
            });
        } else {
            socket.emit('erasemessage', {'status': 'error', 'reason': 'notfound'});
        }
    });

}

function getRoomslist(param) {
    var socket = this;
    var user = socket.profile.login;

    if (!socket.isLogin) {
        socket.emit('getroomslist', {'type': 'getroomslist', 'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    var roomslist = {};

    dbrooms.find({'hidden': 0}, {sort: {'userscount': -1}}).toArray(function (err, res) {
        for (var i = 0; i < res.length; i++) {
            roomslist[res[i]['name']] = {
                'caption': res[i]['caption'],
                'userscount': res[i]['userscount'],
                'topic': res[i]['topic'],
                'owner': res[i]['owner']
            }
        }
        socket.emit('getroomslist', {'status': 'ok', 'list': roomslist});
    });

}

function getHistory(param) {
    var socket = this;
    var room = pWrap(param['room']);
    var skip = parseInt(pWrap(param['skip']));
    var count = parseInt(pWrap(param['count']));
    var user = socket.profile.login;

    if (!socket.isLogin) {
        socket.emit('gethistory', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    var base = [];
    dbhist.find({'room': room, 'text': {$exists: true}, 'deleted': {$exists: false}}, {
        sort: {'date': -1},
        skip: skip,
        limit: count
    }).toArray(function (err, res) {
        for (var i = 0; i < res.length; i++) {
            var ms = {
                'u': res[i]['user'],
                'd': res[i]['date'],
                'c': res[i]['cl'],
                'r': res[i]['room'],
                't': res[i]['text'],
                'id': res[i]['_id']
            };
            ms.n = login2nick[res[i]['user']];
            base.push(ms);
        }
        socket.emit('gethistory', {'status': 'ok', 'room': room, 'messages': base});
    });

}


function pWrap(par) {
    return isset(par) ? par : '';
}

function escapeHtml(unsafe) {
    return unsafe
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;");
}

exports.createRoom = createRoom;
exports.joinRoom = joinRoom;
exports.leaveRoom = leaveRoom;
exports.chat = chat;
exports.bot_chat = bot_chat;
exports.chatcorrect = chatcorrect;
exports.leaveAllRooms = leaveAllRooms;
exports.settopic = setTopic;
exports.eraseMes = eraseMes;
exports.getroomslist = getRoomslist;
exports.gethistory = getHistory;