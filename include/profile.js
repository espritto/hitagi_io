var im = require('imagemagick');
var config = require('../configs/config.js');
var fs = require("fs");
var tools = require('./tools.js');
var ObjectID = require('mongodb').ObjectID;
var _ = require('underscore');

function setProfile(param) {
    var socket = this;
    var puser = pWrap(param['user']);
    var udata = isset(param['userdata']) ? param['userdata'] : {};

    if (!socket.isLogin) {
        sendErrorSet('notlogin', socket);
        return false;
    }

    var priv = socket.profile['privilege'];
    if (priv == 3) {
        sendErrorSet('notallowed', socket);
        return false;
    }
    if (priv == 2 && socket.profile.login != puser) {
        sendErrorSet('notallowed', socket);
        return false;
    }

    if (isset(param['visible'])) {
        var v = param['visible'];
        if (!(v == 0 || v == 1 || v == 2)) {
            sendErrorSet('wrongvisible', socket);
            return false;
        }
    }

    var pdata = {};
    if (isset(udata['gender'])) pdata['gender'] = udata['gender'];
    if (isset(udata['birthday'])) pdata['birthday'] = udata['birthday'];
    if (isset(udata['realname'])) pdata['realname'] = udata['realname'];
    if (isset(udata['country'])) pdata['country'] = udata['country'];
    if (isset(udata['email'])) pdata['email'] = udata['email'];
    if (isset(udata['homepage'])) pdata['homepage'] = udata['homepage'];
    if (isset(udata['phone'])) pdata['phone'] = udata['phone'];
    if (isset(udata['icq'])) pdata['icq'] = udata['icq'];
    if (isset(udata['skype'])) pdata['skype'] = udata['skype'];
    if (isset(udata['twitter'])) pdata['twitter'] = udata['twitter'];
    if (isset(udata['facebook'])) pdata['facebook'] = udata['facebook'];
    if (isset(udata['vk'])) pdata['vk'] = udata['vk'];
    pdata['profile_visible'] = v;

    dbusers.findOne({'login': puser}, function (err, res) {
        if (res) {
            dbusers.updateById(res['_id'], {$set: pdata}, function (err, result) {
            });
            socket.emit('setprofile', {'status': 'ok'});
        } else {
            sendErrorSet('notexists', socket);
        }
    });

}

function getProfile(param) {
    var socket = this;
    var puser = pWrap(param['user']);
    var priv = socket.profile.privilege;

    if (!socket.isLogin) {
        sendErrorGet('notlogin', socket);
        return false;
    }

    dbusers.findOne({'login': puser}, function (err, res) {
        if (res) {
            var vis = res['profile_visible'];

            if (priv == 3 && vis != 0) {
                sendErrorGet('notallowed', socket);
                return false;
            }
            if (priv == 2 && vis == 2 && socket.profile['login'] != puser) {
                sendErrorGet('notallowed', socket);
                return false;
            }

            var pdata = {};
            if (isset(res['nick'])) pdata['nickname'] = res['nick'];
            if (isset(res['gender'])) pdata['gender'] = res['gender'];
            if (isset(res['birthday'])) pdata['birthday'] = res['birthday'];
            if (isset(res['realname'])) pdata['realname'] = res['realname'];
            if (isset(res['country'])) pdata['country'] = res['country'];
            if (isset(res['email'])) pdata['email'] = res['email'];
            if (isset(res['homepage'])) pdata['homepage'] = res['homepage'];
            if (isset(res['phone'])) pdata['phone'] = res['phone'];
            if (isset(res['icq'])) pdata['icq'] = res['icq'];
            if (isset(res['skype'])) pdata['skype'] = res['skype'];
            if (isset(res['twitter'])) pdata['twitter'] = res['twitter'];
            if (isset(res['facebook'])) pdata['facebook'] = res['facebook'];
            if (isset(res['vk'])) pdata['vk'] = res['vk'];

            socket.emit('getprofile', {
                'status': 'ok',
                'user': puser,
                'privilege': res['privilege'],
                'userdata': pdata,
                'visible': vis
            });
        } else {
            sendErrorGet('notexists', socket);
        }
    });

};

function setNick(param) {
    var socket = this;
    var nNick = pWrap(param['nick']);
    var user = socket.profile.login;

    if (!socket.isLogin) {
        socket.emit('setnick', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    if (nNick.length < 3 || nNick.length > 15) {
        socket.emit('setnick', {'status': 'error', 'reason': 'wrongnick'});
        return false;
    }

    if (time() - socket.profile.nickdate < parseInt(config['nickTimeout'])) {
        socket.emit('setnick', {'type': 'setnick', 'status': 'error', 'reason': 'timeout'});
        return false;
    }

    dbusers.find({'nick': nNick}, ['nick']).toArray(function (err, result) {
        if (result.length == 0) {

            login2nick[user] = nNick;
            socket.profile.nickdate = time();
            dbusers.update({'login': user}, {$set: {'nick': nNick, 'nick_date': time()}}, function (err, result) {
            });
            _.each(socket.roomsList(), function (room) {
                socket.to(room).emit('setnick', {'status': 'ok', 'user': user, 'newnick': nNick});
            });

            dbhistEv.insert({'user': user, 'event': 8, 'text': nNick, 'date': time()}, function (err, res) {
            });

        } else {
            socket.emit('setnick', {'status': 'error', 'reason': 'busynick'});
        }
    });

}

function setStatus(param) {
    var socket = this;
    var text = pWrap(param['text']);
    var user = socket.profile.login;
    if (!socket.isLogin) {
        socket.emit('setstatus', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }
    text = strip_tags(text.substring(0, 90));

    dbusers.update({'login': user}, {$set: {'statustext': text}}, function (err, result) {});
    _.each(socket.roomsList(), function (room) {
        io.to(room).emit('setstatus', {'status': 'ok', 'user': user, 'text': text});
    });
    dbhistEv.insert({'user': user, 'event': 6, 'text': text, 'date': time()}, function (err, res) {});
}

function awayStatus(param) {
    var socket = this;
    var status = pWrap(param['status']);
    var user = socket.profile.login;
    if (!socket.isLogin) {
        socket.emit('awaystatus', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    socket.profile.awayStatus = status;

    dbusers.update({'login': user}, {$set: {'awaystatus': status}}, function (err, result) {
    });
    _.each(socket.roomsList(), function (room) {
        io.to(room).emit('awaystatus', {'status': 'ok', 'user': user, 'val': status});
    });

}

function setState(param) {
    var socket = this;
    var val = pWrap(param['val']) * 1;
    var user = socket.profile.login;
    if (!socket.isLogin) {
        socket.emit('setstate', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }
    dbusers.update({'login': user}, {$set: {'state': val}}, function (err, result) {
    });
    _.each(socket.roomsList(), function (room) {
        io.to(room).emit('setstate', {'status': 'ok', 'user': user, 'val': val});
    });
    dbhistEv.insert({'user': user, 'event': 7, 'value': val, 'date': time()}, function (err, res) {
    });
}

function saveRating(param) {
    var socket = this;
    var val = pWrap(param['val']) * 1;
    var mid = pWrap(param['mid']);
    var vuser = socket.profile.login;

    if (!socket.isLogin) {
        socket.emit('saverating', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }
    if (mid == '') {
        socket.emit('saverating', {'status': 'error', 'reason': 'nomid'});
        return false;
    }

    dbhist.findOne({'_id': new ObjectID(mid)}, function (err, res) {
        if (res) {

            if (in_array(vuser, res.voted)) {
                socket.emit('saverating', {'status': 'error', 'reason': 'alreadyvoted'});
                return false;
            }
            if (vuser == res.user) {
                socket.emit('saverating', {'status': 'error', 'reason': 'isowner'});
                return false;
            }

            dbusers.update({'login': res.user}, {$inc: {'rating': val}}, function (err, result) {
                dbusers.findOne({'login': res.user}, function (err, res2) {
                    socket.emit('saverating', {
                        'status': 'ok',
                        'user': res.user,
                        'rating': res2.rating,
                        'nick': res2.nick
                    });
                });
            });

            dbhist.update({'_id': new ObjectID(mid)}, {$addToSet: {'voted': vuser}}, function (err, resu) {
            });

        } else {
            socket.emit('saverating', {'status': 'error', 'reason': 'nomessage'});
        }
    });

}


function getLinkedAccounts(param) {
    var socket = this;
    var user = socket.profile;

    if (!socket.isLogin) {
        socket.emit('linkedaccounts', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    dbusers.find({'linked_to': user._id}, ['_id', 'login', 'nick', 'social_type']).toArray(function (err, res) {
        socket.emit('linkedaccounts', res);
    });

}


function in_array(needle, haystack, strict) {
    var found = false, key, strict = !!strict;
    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }
    return found;
}

function sendErrorSet(reason, socket) {
    socket.emit('setprofile', {'status': 'error', 'reason': reason});
}
function sendErrorGet(reason, socket) {
    socket.emit('getprofile', {'status': 'error', 'reason': reason});
}
function pWrap(par) {
    return isset(par) ? par : '';
}
function strip_tags(str) {	// Strip HTML and PHP tags from a string
    return str.replace(/<\/?[^>]+>/gi, '');
}


exports.set = setProfile;
exports.get = getProfile;
exports.setstatus = setStatus;
exports.setstate = setState;
exports.setnick = setNick;
exports.saverating = saveRating;
exports.awaystatus = awayStatus;
exports.linkedaccounts = getLinkedAccounts;