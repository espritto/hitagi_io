
var config = require('../configs/config.js');
var tools = require('./tools.js');
var ObjectID = require('mongodb').ObjectID;
var _ = require('underscore');

/*
*   SESSION
*
*   _id
*   userId
*   token
*   createDate
*   lastDate
*   client
*   online
*
* */

function create(userId, data, callback) {

    var session = {
        user: userId,
        token: tools.createToken(),
        createDate: time(),
        lastDate: time(),
        client: data.client,
        platform: data.platform,
        ip: data.ip,
        online: true
    };

    return dbsessions.insert(session, function(err, res){
        if(err)
            return callback(null);
        callback(null, res.ops[0]);
    });

}

function update(id, data, callback) {

    var session = {
        lastDate: time(),
        client: data.client,
        platform: data.platform,
        ip: data.ip,
        online: true
    };

    return dbsessions.updateById(id, {
        $set: session
    }, callback);

}

function updateOffline(id, callback) {

    var session = {
        online: false
    };

    return dbsessions.updateById(id, {
        $set: session
    }, callback);

}

function clearOnline(callback) {

    return dbsessions.update({}, {
        $set: {online: false}
    }, {
        multi:true
    }, callback);

}

function remove(id, callback) {

    return dbsessions.remove({_id: id}, callback);

}

function getByUser(userId, callback) {

    return dbsessions.find({user: userId}).toArray(callback);

}

function checkIsOneSession(userId, callback) {

    return dbsessions.find({user: userId, online: true}).toArray(function(err, sessions){
        if(_.size(sessions) == 1)
            return callback();
    });

}

function removeByUser(userId, callback) {

    return dbsessions.remove({user: userId}, callback);

}

function userByToken(token, callback) {

    return dbsessions.findOne({token: token}, function(err, session){
        if(!session || err)
            return callback('session not found', null);
        dbusers.findById(session.user, function(err, user){
            if(!user || err)
                return callback('user not found', null);
            user.currentSession = session;
            callback(null, user);
        });
    });

}


exports.create = create;
exports.update = update;
exports.remove = remove;
exports.getByUser = getByUser;
exports.removeByUser = removeByUser;
exports.userByToken = userByToken;
exports.checkIsOneSession = checkIsOneSession;
exports.updateOffline = updateOffline;
exports.clearOnline = clearOnline;
