
var tools = require('./tools.js');
var config = require('../configs/config.js');
var sessions = require('./sessions.js');


function sendMessage(param) {
    var socket = this;
    var text = pWrap(param['text']);
    var user_to = pWrap(param['to']);
    var color = pWrap(param['cl']);
    var user_from = socket.profile.login;

    if (user_from == user_to) {
        socket.emit('sendmess', {'status': 'error', 'reason': 'tomyself'});
        return false;
    }

    if (!socket.isLogin) {
        socket.emit('sendmess', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    if (text.length == 0 || text.length > config['messagePrivateMaxLength']) {
        socket.emit('sendmess', {'status': 'error', 'reason': 'badtext'});
        return false;
    }

    dbusers.findOne({'login': user_to}, function (err, res) {
        if (res) {
            var toSend = {
                'status': 'ok',
                'message': {
                    'u': user_from,
                    't': text,
                    'cl': color,
                    'd': time(),
                    'n': socket.profile.nick,
                    'r': user_to
                }
            };


            var ins = {
                'to': user_to,
                'from': user_from,
                'text': text,
                'status': 1,
                'date': time()
            };
            dbmess.insert(ins, function (err, res) {});
            toSend.message.id = ins['_id'];


            io.to('pm_' + user_to).emit('recmes', toSend);
            io.to('pm_' + user_from).emit('recmes', toSend);
            //socket.emit('recmes', toSend);


            //if(res['socket'] != '' || true){
            //	var ins = {'to':user_to, 'from':user_from, 'text':text, 'status':1, 'date':time()};
            //	dbmess.insert(ins, function(err,res){});
            //	toSend.message.id = ins['_id'];
            //	socket.to('pm_' + user_to).emit('recmes', toSend);
            //	socket.emit('recmes', toSend);
            //	console.log("varint 1", ins);
            //} else {
            //	var ins = {'to':user_to, 'from':user_from, 'text':text, 'status':0, 'date':time()};
            //	dbmess.insert(ins, function(err,res){});
            //	toSend.message.id = ins['_id'];
            //	socket.emit('recmes', {'status':'ok', 'from':user_from, 'text':text, 'cl':color, 'date':time()});
            //	console.log("varint 2", ins);
            //}
        } else {
            socket.emit('sendmess', {'status': 'error', 'reason': 'notfound'});
        }

    });
}
function checkNewMessages(socket) {
    var isRecive = false;
    var user = socket.profile.login;

    dbmess.find({'to': user, 'status': 0}).toArray(function (err, res) {
        for (var i = 0; i < res.length; i++) {
            isRecive = true;
            socket.emit('recmes', {'from': res[i]['from'], 'text': res[i]['text'], 'date': res[i]['date']});
        }
        if (isRecive) {
            dbmess.update({'to': user, 'status': 0}, {$set: {'status': 1}}, {'multi': true}, function (err, res) {
            });
        }
    });

}
function getMessages(param) {
    var socket = this;
    var user = pWrap(socket.profile.login);
    var from = pWrap(param['from']);
    var count = parseInt(pWrap(param['count']));

    if (!socket.isLogin) {
        socket.emit('sendmess', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    var dat = [];
    dbusers.findOne({'login': from}, function (err, res) {
        if (res) {
            var fNick = res['nick'];
            var friend = {
                'login': from,
                'nick': fNick,
                'avaurl': res['ava_index'] ? '/avatars/' + from + '.jpg?' + res['ava_index'] : '/avatars/noavatar.gif',
                'statustext': res['statustext'],
                'state': res['state'],
                'commpriv': res['privilege']
            };
            var criteria = {'$or': [{to: user, from: from}, {to: from, from: user}]};
            dbmess.find(criteria, {limit: count, sort: {'date': -1}}).toArray(function (err, res) {
                for (var i = 0; i < res.length; i++) {
                    var m = {
                        'u': res[i]['from'],
                        't': res[i]['text'],
                        'n': (res[i]['from'] == user) ? socket.profile.nick : fNick,
                        'd': res[i]['date'],
                        'id': res[i]['_id']
                    };
                    dat.push(m);
                }
                socket.emit('getmessages', {'status': 'ok', 'user': friend, 'messages': dat.reverse()});
            });
        } else {
            socket.emit('sendmess', {'status': 'error', 'reason': 'notfound'});
        }
    });


}


function getPMusers(param) {
    var socket = this;
    var user = pWrap(socket.profile.login);

    if (!socket.isLogin) {
        socket.emit('getpmusers', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

}

function getAdminMessages(param) {
    var socket = this;
    var user = pWrap(param['user']);
    var order = pWrap(param['order']);

    if (user == '') {
        socket.emit('getmessages', {'status': 'error', 'reason': 'nouser'});
        return false;
    }
}

function pWrap(par) {
    return isset(par) ? par : '';
}

exports.sendMessage = sendMessage;
exports.getPMusers = getPMusers;
exports.getMessages = getMessages;