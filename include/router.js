var config = require('../configs/config.js');
var rega = require('./register.js');
var auth = require('./auth.js');
var profile = require('./profile.js');
var mess = require('./messages.js');
var room = require('./rooms.js');
var priv = require('./priveleges.js');

function router(socket) {
    socket.on('getserverinfo',      serverInfo);
    socket.on('register',           rega.registerUser);
    socket.on('adminreg',           rega.adminReg);

    socket.on('auth',               auth.authByToken);      // авторизация по токену используется при перезапуске!
    socket.on('login',              auth.logIn);            // old auth
    socket.on('loginsocial',        auth.authSocial);       // old authsocial
    socket.on('logout',             auth.logOut);
    socket.on('deluser',            auth.delUser);
    socket.on('blockuser',          auth.blockUser);
    socket.on('linksocial',         auth.linkSocial);
    socket.on('unlinksocial',       auth.unlinkSocial);

    socket.on('sendmess',           mess.sendMessage);
    socket.on('getmessages',        mess.getMessages);
    socket.on('getpmusers',         mess.getPMusers);

    socket.on('createroom',         room.createRoom);
    socket.on('joinroom',           room.joinRoom);
    socket.on('leaveroom',          room.leaveRoom);
    socket.on('chat',               room.chat);
    socket.on('chatcorrect',        room.chatcorrect);
    socket.on('settopic',           room.settopic);
    socket.on('gethistory',         room.gethistory);
    socket.on('getroomslist',       room.getroomslist);
    socket.on('erasemessage',       room.eraseMes);

    socket.on('getprofile',         profile.get);
    socket.on('setprofile',         profile.set);
    socket.on('setstatus',          profile.setstatus);
    socket.on('setstate',           profile.setstate);
    socket.on('saverating',         profile.saverating);
    socket.on('setnick',            profile.setnick);
    socket.on('awaystatus',         profile.awaystatus);
    socket.on('linkedaccounts',     profile.linkedaccounts);

    socket.on('banon',              priv.banon);
    socket.on('banoff',             priv.banoff);
    socket.on('voiceon',            priv.voiceon);
    socket.on('voiceoff',           priv.voiceoff);
    socket.on('kick',               priv.kick);
    socket.on('globprivilege',      priv.globpriv);
    socket.on('roomprivilege',      priv.roompriv);
}

/************************* Server Info ********************/
function serverInfo(data, cb) {

    var socket = this;

    var dat = {
        'status': 'ok',
        'servername': config['serverName'],
        'serverversion': config['version'],
        'author': config['author'],
        'homepage': config['homepage'],
        'uptime': time() - startTime
    };

    dbusers.find().count(function (err, result) {
        dat['totalusers'] = result;
        db.collection('rooms').find().count(function (err, result) {
            dat['totalrooms'] = result;
            socket.emit('getserverinfo', dat);
        });
    });
    return true;
}


module.exports.rout = router;