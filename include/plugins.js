var fs = require("fs");
var xml2js = require('xml2js');
var http = require('http');
var async = require('async');
var moment = require('moment');
var humanizeDuration = require('humanize-duration');
var url = require('url');
var config = require('../configs/config.js');
var priveleges = require('./priveleges.js');
var tools = require('./tools.js');
//var gameApp = require('../plugins/game_app.js');
var _ = require('underscore');

var helpFile = '';
var antiRegs = {};

var victorinaQuestions = [];
var victorinaAnswers = [];
var currentQuest = 0;
var wrongCount = 0;
var vicBall = 3;
var helpWord = '';
var baseName = 'vic_baza2';

var butActs = [];
var ruletLevels, ruletTimes, ruletTimesString;
var ruletCurrent = 0;

_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

exports.load = function () {

    TestovichLoad();
    VictorinaLoad();
    //gameApp.load();

};


/* db */

var dataProvider = {
    testovich: {
        get: function(cb){
            dbplugins.findOne({'bot': 'Testovich'}, function (err, res) {
                return cb(res.data);
            });
        },
        set: function(data){
            dbplugins.update({'bot': 'Testovich'}, {$set: {'data': data}}, function (err, result) { });
        }
    }
};

exports.bot = function (sock, rec, callback) {

    // настройки по-умолчанию
    rec.save = true;	// сохранить в истории
    rec.toAll = true;	// отправить всем
    rec.isBot = false;	// сообщение не ботом НЕ обработано
    rec.s = sock;

    //быстрая вставка ответов для викторины
    rec.text = rec.text.replace(/^!!([а-яА-Я]+)/gi, '!о $1');

    rec = Antimat(rec);
    var rg = /^!([а-яА-Я_]+) (.*)/gi;
    var f = rg.exec(rec.text + ' ');
    if(f) {       // если успользуется команда бота

        async.waterfall([
            function(cb) {
                Testovich(f, rec, cb);
            },
            function(_rec, cb) {
                Victorina(f, _rec, cb);
            }
        ], function (err, _rec) {
            callback(_rec);
        });

    } else {
        callback(rec);
    }

};

/* ************************************************************************* */
function TestovichLoad() {
    fs.readFile(tools.projectPath() + '/plugins/testbot_help.html', function (err, data) {
        helpFile = nl2br(String(data));
    });

    //antiRegs.regs = [
    //    new RegExp("[(\s|_)]*(x|х|h)+?[(\s|_)]*(y|u|у)+?[(\s|_)]*([йиy]|[яyеeё])+?[(\s|_)]*(л|м|m)*?[(\s|_)]*", "gi"),
    //    new RegExp("[(\s|_)]*[xхh]+[(\s|_)]*[yuу]+[(\s|_)]*[Л]+[(\s|_)]*[иеui]", "gi"),
    //    new RegExp("[(\s|_)]*[пpn]+[(\s|_)]*[иеeёiu]+[(\s|_)]*[зсcsz3]+[(\s|_)]*[дd]+[(\s|_)]*", "gi"),
    //    new RegExp("[(\s|_)]*[бb6]+[(\s|_)]*[лl]+[(\s|_)]*([яy9])+[(\s|_)]*([дd]|[тt])*", "gi"),
    //    new RegExp("[(\s|_)]*([иiu]|[еeё])+[(\s|_)]*([п]|[бb6])+[(\s|_)]*(л)+[(\s|_)]*(а|и|[й]|о)+[(\s|_)]*", "gi"),
    //    new RegExp("[(\s|_)]*([иiu]|[еeё])+[(\s|_)]*([п]|[бb6])+[(\s|_)]*([аa@]|[o0о])+[(\s|_)]*(н|т|л)+", "gi"),
    //    new RegExp("[(\s|_)]*([иiu]|[еeё])+[(\s|_)]*([п]|[бb6])+[(\s|_)]*(и)+[(\s|_)]*(с|т)+", "gi"),
    //    new RegExp("[(\s|_)]*([уy])+[(\s|_)]*([иiu]|[еeё])+[(\s|_)]*([п]|[бb6])+[(\s|_)]*([аa@]|[o0о])+", "gi"),
    //    new RegExp("[(\s|_)]*(з|3|s|z)+?[(\s|_)]*(а|о|a|o|0)+?[(\s|_)]*[лl]+?[(\s|_)]*(у|u|y)+?[(\s|_)]*(п|p|n)+?[(\s|_)]*", "gi"),
    //    new RegExp("[(\s|_)]*(д)+?[(\s|_)]*(о|а|o|a|0)+?[(\s|_)]*(Л)+?[(\s|_)]*(б|п)+?[(\s|_)]*(а|о|a|o|0)+?[(\s|_)]*(е|ё)+?[(\s|_)]*(б|п)+?[(\s|_)]*", "ig")
    //];
    antiRegs.regs = [
        new RegExp("[(\s|_)]*(х)+?[(\s|_)]*(у)+?[(\s|_)]*([йи]|[яеё])+?[(\s|_)]*(л|м)*?[(\s|_)]*", "gi"),
        new RegExp("[(\s|_)]*[х]+[(\s|_)]*[у]+[(\s|_)]*[Л]+[(\s|_)]*[ие]", "gi"),
        new RegExp("[(\s|_)]*[п]+[(\s|_)]*[иеё]+[(\s|_)]*[зс3]+[(\s|_)]*[д]+[(\s|_)]*", "gi"),
        new RegExp("[(\s|_)]*[б6]+[(\s|_)]*[л]+[(\s|_)]*([я9])+[(\s|_)]*([д]|[т])*", "gi"),
        new RegExp("[(\s|_)]*([и]|[её])+[(\s|_)]*([п]|[б6])+[(\s|_)]*(л)+[(\s|_)]*(а|и|[й]|о)+[(\s|_)]*", "gi"),
        new RegExp("[(\s|_)]*([и]|[её])+[(\s|_)]*([п]|[б6])+[(\s|_)]*([а@]|[0о])+[(\s|_)]*(н|т|л)+", "gi"),
        new RegExp("[(\s|_)]*([и]|[её])+[(\s|_)]*([п]|[б6])+[(\s|_)]*(и)+[(\s|_)]*(с|т)+", "gi"),
        new RegExp("[(\s|_)]*([у])+[(\s|_)]*([и]|[её])+[(\s|_)]*([п]|[б6])+[(\s|_)]*([а@]|[0о])+", "gi"),
        new RegExp("[(\s|_)]*(з|3)+?[(\s|_)]*(а|о|0)+?[(\s|_)]*[л]+?[(\s|_)]*(у)+?[(\s|_)]*(п)+?[(\s|_)]*", "gi"),
        new RegExp("[(\s|_)]*(д)+?[(\s|_)]*(о|а|0)+?[(\s|_)]*(Л)+?[(\s|_)]*(б|п)+?[(\s|_)]*(а|о|0)+?[(\s|_)]*(е|ё)+?[(\s|_)]*(б|п)+?[(\s|_)]*", "ig")
    ];

    antiRegs.exeptions = [
      '((за)|(под))?страхуй(те)?',
    '(за)?штрихуй(те)?',
    '^(анти|пере)?бан(а|е|я|у)*',
    'степан(дос(а|у)?)?',
    '(не)?(\s)*бан([ияю]т?(ся?)|(ь)?те)',
    '(не)?психуй(те)?',
    'колебани(я|е|ю|й)',
    '((полу)?са|[оа]гло|гре|к[оа]ра|ру|сте|ансам)бля(ми)',
    'бля(ха|хи|хой|ами|шка)',
    '([оа]сл[оа]|[оа]ск[оа]р|п[оа]дс[ао]|п[оа]с(л)?[оа]|п[оа]тр[еи]|углу|(пр[еи])?усугу|разгра|рас(с)?ла|пр[еи]сп[оа]с[оа]|уп[оа]д[оа]|употре|истр[еи]|влю|зл[оа]уп[оа]тр[еи]|[оа]б[оа]с[оа]|[оа]зл[оа])бля(л|ть|е([шщ][ьъ]?|т(е)?|м(ое|ая)?)|ю|й|ют([ьъ])?(ся)?)',
    '(за)?страх(а|и|ов(о(й)?|ые|ая|ка|ать|[оа]н(н)?(ая|а|ы(е|й)?)?))?',
    'хулиган(ит(ь)?|ск(ий|ая|ое|ие)ств(о(вать)?|а)|ь[её])?',
    '(хл|угр|соскр|сгр|(ра(з|с)|пр(и|о)|от|под|по|на|пере|до|вз)?гр|(по)?кол|захл|ск|изг)(е|и)бать',
    'гр(е|ё)бан(н)?(ый|ая|(о|ы)е)',
    '(за|от|в|на|при)липать',
    'типа',
    'телепат(ия)?',
    'трепа(ния|т)',
    'н(и|е)п(о|а)нятн(о|а)',
    '(мн|теб|т)е[\s]+бан',
    'д(е|и)р(е|и)жабля',
    'сно(к|г)(с)?ш(е|и)бат(е|и)льн(н)?(о|ый|ая|ое)',
    '((о|а)т|у)?(ш|щ)л(е|ё)па(й|т|н)',
    '^потреп(п)?аться',
    '^бан',
    'дебат(ы|ов|ами)',
    'н(е|и)п(о|а)луч(и|е)т',
    'кордебал(\s)*',
    '(г)?ипот(\s)*',
    '(и|е|я)пон(\s)*',
    '(н(а)?|пр)(и|е)б(о|а)л(ьш|е)',
    'н(е|и)(\s)*пон(я|И)',
    'з(а|о)к(а|о)л(и|е)бал',
    'рибонук',
    'муницип(\s)*',
    'т(е|ё)пл',
    'закипал',
    'не(\s)*пал',
    'не(\s)*благодар',
    'перепись',
    'пр(и|е)писыв(\s)*',
    'потребител(ь|ьская|ьский)',
    'дипло(м|мы|мом|мский|ма)',
    'пр(е|и)л(е|и)пить',
    'библ(\s)*',
    'ош(е|и)(п|б)(лась|лись|ся)',
    'ош(е|и)б(и|а)(л|ть)(\s)*',
    'дисциплин(\s)*',
    'неплох(\s)*',
    '(при|пере)бит(ь|ый)',
    'п(е|и)р(е|и)пал(\s)*',
    '(за)?кол(е|и)б(\s)*',
    'гребл(\s)*',
    'пеплом',
    '(по|за)?гибл(\s)*',
    'небли(\s)*',
    'неплод(\s)*',
    'закип(\s)*',
    'непол(\s)*',
    'пере(п|б)(\s)*',
    'пр(е|и)бли(з|ж)(\s)*',
    'талиб(\s)*',
    'кипит',
    'слепит',
    'общепи(\s)*',
    '(по|за)?тереб(\s)*',
    'еписко(\s)*',
    'залип(\s)*',
    'ип(а|о)тек(\s)*',
    '(c)?л(е|и)пота(\s)*',
    '(за)?креп(\s)*',
    'сшиб(ал)?и(\s)*',
    '(к|г)а(н|нн)ибал(\s)*',
    'т(е|и)р(е|и)бон(\s)*',
    'пр(и|е)писа(\s)*',
    'п(ере|ри)полз(\s)*',
    'гибон(\s)*',
    'ипатк(\s)*',
    'ипполит(\s)*',
    'хрипл(\s)*',
    'всеблаг(\s)*',
    'встрепан(\s)*',
    'выцеп(\s)*',
    'гепатит(\s)*',
    'загибал(\s)*',
    'затрепал(\s)*',
    'захлипал(\s)*',
    'зацепи(\s)*',
    'зашлепал(\s)*',
    'истребит(\s)*',
    'кибит(\s)*',
    'копипаст(\s)*',
    'корабл(\s)*',
    'меблиров(\s)*',
    'нагиба(\s)*',
    'нащипан(\s)*',
    'неблагопол(\s)*',
    'неблагораз(\s)*',
    'неблагород(\s)*',
    'неписан(\s)*',
    'огибал(\s)*',
    'оглобля(\s)*',
    'перегиб(\s)*',
    'погиба(\s)*',
    'потрепа(\s)*',
    'прибалт(\s)*',
    'принципа(\s)*',
    'припа(\s)*',
    'раздробля(\s)*',
    'ра(с|сс)трепа(\s)*',
    'расшибл(\s)*',
    'рубл(\s)*',
    'сабл(\s)*',
    'сидябляк(\s)*',
    'трепал(\s)*',
    'углубля(\s)*',
    'употреб(\s)*',
    'хрипло',
    'шепот(\s)*',
    'шлепал(\s)*',
    'щепот(\s)*',
    'щипа(\s)*',
    'щипан(\s)*',
    'бронеплас(\s)*',
    'влипл(\s)*',
    'внеплан(\s)*',
    'вцепит(\s)*',
    'выгибал(\s)*',
    'вылеп(\s)*',
    'вышиб(\s)*',
    '(за|)скреб(\s)*',
    '(за|)трепан(\s)*',
    'зашиби(\s)*',
    'изгибал(\s)*',
    'икебан(\s)*',
    'истребл(\s)*',
    'истрепа(\s)*',
    'констебл(\s)*',
    'лепит(\s)*',
    'липл(\s)*',
    'мультиплик(\s)*',
    'неблагоприятн(\s)*',
    'неплотн(\s)*',
    'непотреб(\s)*',
    'обтрепан(\s)*',
    'огиба(\s)*',
    'ослепл(\s)*',
    'отцеп(\s)*',
    'оцеп(\s)*',
    'пепла',
    'погребальн(\s)*',
    'подгиба(\s)*',
    'подцепи(\s)*',
    'потребля(\s)*',
    'прибалде(\s)*',
    'прибит(\s)*',
    'пригиба(\s)*',
    'прилипл(\s)*',
    'припис(\s)*',
    'приплати(\s)*',
    'приплачивать',
    'приполярн(\s)*',
    'пришиб(\s)*',
    'проскреб(\s)*',
    'разгиб(\s)*',
    'разгреб(\s)*',
    'реплик(\s)*',
    'сгибан(\s)*',
    'сгреба(\s)*',
    'сипл(\s)*',
    'скрип(\s)*',
    'слепот(\s)*',
    'слипал(\s)*',
    'слипли(\s)*',
    'стебл(\s)*',
    'типологическ(\s)*',
    'треп(\s)*',
    'уцепить(\s)*',
    'хлеба(\s)*',
    'хрипи(\s)*',
    'чаепит(\s)*',
    'шибанул(\s)*',
    'шипит',
    'двенадцатибалльн(\s)*',
    'мореплава(\s)*',
    'прилипа(\s)*',
    'ра(сс|с)лабля(\s)*',
    'сногсшибат(\s)*',
    'трипол(\s)*',
    'усугуб(\s)*',
    'ушибл(\s)*'
    ];
    antiRegs.exeptions = _.map(antiRegs.exeptions, function(item){
        return new RegExp(item, 'i');
    });

    ruletLevels = ['Школоло *smile87*', 'Испытатель *smile45*', 'Мужик *smile84* ', 'КГБшник *smile53*', 'Терминатор *smile38*', 'Аццкий Сотона  *smile58*'];
    ruletTimes = [0, 5, 15, 60, 360, 1440];
    ruletTimesString = ['', 'Покинул нас всего на 5 минут', 'Мы не увидим тебя аж 15 минут', 'Герой пал смертью храбрых на целый час!', 'Это дерзкий поступок!!! Ты пробудешь в расплавленном железе 6 часов!', 'Преисподняя разверглась и забрала тебя на ЦЕЛЫЕ СУТКИ!!!'];


}
function Testovich(f, rec, callback) {

    var com = f[1].toLowerCase();	// название команды
    var mess = trim(f[2], ' '); 	// текст после команды
    var params = mess.split(' ');	// массив параметров

    if (com == 'я') {
        rec.text = '<b>' + rec.nick + '</b> ' + mess;
        rec.toAll = true;
        rec.isBot = true;
    }
    if (com == 'справка') {
        rec.text = helpFile;
        rec.toAll = false;
        rec.save = false;
        rec.isBot = true;
    }
    if (com == 'нг') {

        var duration = moment([moment().year() + 1, 0, 1]).diff(moment());

        var ng = humanizeDuration(duration, {
            language: 'ru',
            largest: 4
        });
        var ngDays = humanizeDuration(duration, {
            language: 'ru',
            round: true,
            units: ['d']
        });

        rec.text = 'До Нового Года осталось: ' + ng + ' или ' + ngDays;
        rec.toAll = true;
        rec.isBot = true;
    }
    if (com == 'песня') {
        rec.text = rec.nick + ' предлагает послушать <a target="_blank" href="http://vk.com/audio?q=' + encodeURIComponent(mess) + '">' + mess + '</a>';
        if (mess == '') rec.text = 'Введите название песни, например: !песня страна лимония';
        rec.toAll = true;
        rec.isBot = true;
    }
    if (com == 'монета') {
        if (Math.random() < 0.95) {
            if (Math.random() > 0.5) {
                rec.text = '<b>' + rec.nick + '</b> бросил монету, выпал ОРЕЛ';
            } else {
                rec.text = '<b>' + rec.nick + '</b> бросил монету, выпала РЕШКА';
            }
        } else {
            // пасхал
            rec.text = '<b>' + rec.nick + '</b> бросил монету, встала на РЕБРО';
        }
        rec.toAll = true;
        rec.isBot = true;
    }
    if (com == 'кубик') {
        if (Math.random() < 0.90) {
            rec.text = '<b>' + rec.nick + '</b> бросил кубик, результат: ' + (Math.floor(Math.random() * 6) + 1);
        } else {
            var choice = Math.random();
            if (choice <= 0.125) {
                rec.text = rec.nick + ', у тебя НОЛЬ, неудачник!';
            } else if (choice <= 0.250) {
                rec.text = rec.nick + ', ты выйграл ПРИЗ! <img src="http://s3.zerochan.net/Yokune.Ruko.240.267453.jpg"/>';
            } else if (choice <= 0.375) {
                rec.text = rec.nick + ', сделай ЭТО нежно!!! <img src="http://i4.imageban.ru/thumbs/2011.01.04/ea60e305f4b70adfdbb33cefc10eaa77.png"/>';
            } else if (choice <= 0.500) {
                rec.text = rec.nick + ', пошли в туалет!!! <img src="http://oi39.tinypic.com/21cvw2v.jpg"/>';
            } else if (choice <= 0.625) {
                rec.text = rec.nick + ', хочешь ласки? <img src="http://oi42.tinypic.com/53tnk8.jpg"/>';
            } else if (choice <= 0.750) {
                rec.text = rec.nick + ', выйграл яндере!!! <img src="http://iichan.hk/b/arch/thumb/1275927138571s.jpg"/>';
            } else if (choice <= 0.875) {
                rec.text = rec.nick + ', ты следующий!!! <img src="http://th1133.photobucket.com/albums/m589/HanakoKatsu101/cut%20veins/th_murderonkenblossumave.jpg"/>';
            } else {
                rec.text = rec.nick + ', позаботься обо мне!!! <img src="http://worldschooluniform.ru/_ph/24/1/846171072.jpg"/>';
            }
        }
        rec.toAll = true;
        rec.isBot = true;
    }
    if (com == 'бутылка') {

        var userList = _.map(io.socketsInRoom(rec.room), function (s) {
            return s.profile;
        });
        userList = _.filter(userList, function (user) {
            return user.login != rec.s.profile.login;
        });

        if (userList.length > 1) {                  // для запуска требуется более 1 свободного юзера

            return dataProvider.testovich.get(function(storage){

                var choice = userList[_.random(0, userList.length - 1)] || {nick: 'FRIEND'};

                var bottleSimple = storage.bottle[Math.floor(Math.random() * storage.bottle.length)];
                if(!bottleSimple) bottleSimple = 'тупанули';

                var bottleAction = storage.bottleAction[Math.floor(Math.random() * storage.bottleAction.length)];
                if(!bottleAction) bottleSimple = 'Ничего не произошло';

                var simpleTpl = _.template('<i class="bottle-line"></i>' + '{{me}} крутанул бутылку, она повернулась на {{friend}} и они {{simpleAction}}');
                var placeholders = {
                    me: '<b>' + rec.nick + '</b>',
                    friend: '<b>' + choice.nick + '</b>',
                    number: storage.crashedBottles,
                    bottles: sklonen(storage.crashedBottles, 'бутылка', 'бутылки', 'бутылок'),
                    simpleAction: bottleSimple
                };

                var random = _.random(0, 8);

                if(random == 1) {
                    rec.text = _.template('<i class="bottle-line"></i>' + bottleAction)(placeholders);
                    if(bottleAction.indexOf('{{number}}') > -1) {
                        storage.crashedBottles++;
                        dataProvider.testovich.set(storage);
                    }
                } else {
                    rec.text = simpleTpl(placeholders);
                }

                rec.toAll = true;
                rec.save = true;
                rec.isBot = true;

                callback(null, rec);

            });

        } else {
            rec.text = 'Слишком мало юзеров в комнате для запуска бутылки';
            rec.toAll = false;
            rec.save = false;
            rec.isBot = true;
        }
    }
    if (com == 'рулетка') {
        var choice = Math.random();
        var level = parseInt(params[1]);

        if (mess == 'топ') {


        } else if (params[0] == 'уровень') {

            if (level >= 0 && level <= 5) {
                rec.text = rec.nick + ' изменил уровень рулетки на <b>' + ruletLevels[level] + '</b>';
                ruletCurrent = level;
            } else {
                rec.text = 'Текущий уровень рулетки: ' + ruletLevels[ruletCurrent];
            }

        } else {
            if (choice > 0.16667) {
                rec.text = '<b>' + rec.nick + '</b> сыграл в русскую рулетку на уровене <b>' + ruletLevels[ruletCurrent] + '</b> и <b>ВЫЖИЛ</b>!!!';
            } else {
                rec.text = '<b>' + rec.nick + '</b> сыграл в русскую рулетку уровне <b>' + ruletLevels[ruletCurrent] + '</b> и <b>УМЕР</b>!!!';
                if (ruletCurrent == 0) {
                    priveleges.internalKick(rec.room, rec.s.profile.login);
                } else {
                    priveleges.internalBan(rec.room, rec.s.profile.login, ruletTimes[ruletCurrent], 'Выстрел в голову из револьвера');
                    rec.text = rec.text + '<br>' + ruletTimesString[ruletCurrent];
                }

            }
        }

        rec.toAll = true;
        rec.save = true;
        rec.isBot = true;
    }
    if (com == 'доб') {

        rec.toAll = false;
        rec.save = false;
        rec.isBot = true;

        if (mess == '') {
            rec.text = 'Введите действие для Бутылки';
        } else {

            return dataProvider.testovich.get(function(storage){

                storage.bottle.push(mess);
                rec.text = 'Вы добавили в Бутылку новое действие "... и они ' + mess + '", всего действий: ' + storage.bottle.length;
                dataProvider.testovich.set(storage);
                callback(null, rec);

            });

        }

    }
    if (com == 'действиябутылки') {

        return dataProvider.testovich.get(function(storage){

            rec.text = 'Список всех действий бутылки (для добавления действия используйте !доб, для удаления - !удал номер_действия)<br><br>';
            _.each(storage.bottle, function(item, i){
                rec.text += (i+1) + ' - ' + item + '<br>';
            });
            rec.toAll = false;
            rec.save = false;
            rec.isBot = true;
            callback(null, rec);
        });

    }
    if (com == 'дворецкий') {
        var name = rec.s.profile.login;
        var help = '<br><br> Дворецкий будет встречать вас приветствием когда вы заходите в комнату. Изначально он не знает как вас встретить, но его можно научить используя следующие команды:<br><b>!дворецкий /мое_приветствие/</b> - задать любую фразу <br><b>!дворецкий удалить</b> - приказать дворецкому не приветствовать вас';

        return dataProvider.testovich.get(function(storage) {

            if (mess == '') {
                var txt = storage.prompt[name];
                if (txt) {
                    rec.text = 'Ваш Дворекций встречает вас со словами: ' + txt + help;
                } else {
                    rec.text = 'Ваш Дворекций еще не знает как вас встречать.' + help;
                }
            } else if (mess == 'удалить') {
                storage.prompt[name] = '';
                rec.text = 'Вы приказали Дворецкому замолчать!';
                dataProvider.testovich.set(storage);
            } else {
                storage.prompt[name] = mess;
                storage.prompt_time[rec.s.profile.login] = 1;
                rec.text = 'Вы научили Дворецкого встречать вас со словами: ' + mess;
                dataProvider.testovich.set(storage);
            }

            rec.toAll = false;
            rec.save = false;
            rec.isBot = true;
            callback(null, rec);

        });

    }
    if (com == 'дворецкий_привет') {

        return dataProvider.testovich.get(function(storage) {

            var txt = storage.prompt[rec.s.profile.login];
            var tm = storage.prompt_time[rec.s.profile.login];
            if (!tm) tm = 1;

            if (time() - tm > 43200) {

                if (txt) {
                    rec.text = '<div style="text-align:center;"><img src="http://imgs.su/tmp/2014-01-10/1389303457-432.jpg" alt="" /><br>' + txt + '</div>';
                } else {
                    rec.text = '';
                }

                storage.prompt_time[rec.s.profile.login] = time();
                dataProvider.testovich.set(storage);
            } else {
                rec.text = '';
            }

            rec.toAll = false;
            rec.save = false;
            rec.isBot = true;

            callback(null, rec);

        });

    }
    if (com == 'картинка') {

        rec.toAll = true;
        rec.save = true;
        rec.isBot = true;

        return getPage('http://tools.redspirit.ru/anipics/get_random.php', function (text) {
            rec.text = text;
            callback(null, rec);
        });

    }

    callback(null, rec);
}

/* ************************************************************************* */

function VictorinaLoad() {
    fs.readFile(tools.projectPath() + '/plugins/' + baseName + '.txt', function (err, data) {
        var baza = String(data).split('\n');
        var str;
        for (var i in baza) {
            str = baza[i].split('|');
            victorinaQuestions.push(str[0]);
            victorinaAnswers.push(str[1]);
        }
    });
}
function Victorina(f, rec, callback) {
    if (rec.isBot) return callback(null, rec);
    var com = f[1].toLowerCase();
    var mess = trim(f[2], ' ');
    var params = mess.split(' ');

    if (com == 'топ') {
        var n = 0;
        var list = [];

        return dataProvider.testovich.get(function(storage) {

            for (var i in storage.users) {
                list.push({nick: login2nick[i], val: storage.users[i]});
            }
            list.sort(function (a, b) {
                return b.val - a.val;
            });

            rec.text = '<b>Рейтинг игроков Викторины (ТОП10)</b><br>';

            for (var i in list.slice(0, 10)) {
                n++;
                rec.text += n + '. ' + list[i].nick + ' - ' + list[i].val + '<br>';
            }

            rec.toAll = true;
            rec.save = false;
            rec.isBot = true;

            callback(null, rec);

        });

    }

    if (com == 'викторина') {

        return dataProvider.testovich.get(function(storage) {

            rec.text = '<b>Игра Викторина 1.32</b><br><br>!вопрос - задать новый вопрос. <br>' +
                '!ответ (или !!) - ответить на вопрос, например: !!зяблик. <br>' +
                '!топ - показать список лидеров викторины. <br>' +
                '!выбратьбазу - указать новую базу вопросов.<br><br>' +
                'На ответ дается 3 попытки, после первой попытки игрокам покажется подсказка. ' +
                'Если игрок укажет правильный ответ с первого раза, то он заработает 3 балла. ' +
                'Если со второго раза - 2 балла. Если с третьего, то 1 балл. ' +
                'За неправельные ответы баллы не начисляются и не уменьшаются<br><br>' +
                'Всего вопросов в базе: ' + victorinaQuestions.length +
                '<br>Задано вопросов: ' + storage['quests'] +
                '<br>Дано правильных ответов: ' + storage['answers'];

            rec.toAll = true;
            rec.save = false;
            rec.isBot = true;
            callback(null, rec);

        });

    }
    if (com == 'вопрос') {

        return dataProvider.testovich.get(function(storage) {

            currentQuest = Math.floor(Math.random() * victorinaQuestions.length);
            var latters = _.size(victorinaAnswers[currentQuest]);
            var lattersStr = sklonen(latters, 'буква', 'буквы', 'букв');
            rec.text = '<br><b>Новый вопрос</b>: ' + victorinaQuestions[currentQuest] + ' (' + latters + ' ' + lattersStr + ')';
            storage['quests']++;
            wrongCount = 0;
            vicBall = 3;

            dataProvider.testovich.set(storage);
            rec.toAll = true;
            rec.save = false;
            rec.isBot = true;
            callback(null, rec);

        });

    }
    if (com == 'ответ' || com == 'о') {

        return dataProvider.testovich.get(function(storage) {

            var score = storage.users[rec.s.profile.login];
            if (!score) score = 0;

            if (currentQuest > 0) {
                if (mess.toLowerCase() == victorinaAnswers[currentQuest].toLowerCase()) { // дан правельный ответ
                    var rezult = score + vicBall;
                    storage.users[rec.s.profile.login] = rezult;
                    rec.text = rec.nick + ' ответил: "' + mess + '" - <b>ПРАВИЛЬНО!</b> Ты выиграл ' + vicBall + ' балла. Итого: ' + rezult;
                    storage['answers']++;
                    currentQuest = 0;
                } else {
                    var oldQ = currentQuest;

                    wrongCount++;
                    rec.text = rec.nick + ' ответил: "' + mess + '" - НЕ ВЕРНО! ';

                    if (wrongCount == 1) {
                        vicBall = 2;
                        rec.text += 'Осталось попыток: <b>2</b>. Подсказка: <b>' + makeHelp1(victorinaAnswers[oldQ]) + '</b>';
                    }
                    if (wrongCount == 2) {
                        vicBall = 1;
                        rec.text += 'Осталось попыток: <b>1</b>. Подсказка: <b>' + makeHelp2(victorinaAnswers[oldQ]) + '</b>';
                    }
                    if (wrongCount == 3) {
                        currentQuest = Math.floor(Math.random() * victorinaQuestions.length);
                        var latters = _.size(victorinaAnswers[currentQuest]);
                        var lattersStr = sklonen(latters, 'буква', 'буквы', 'букв');
                        rec.text += ' Правильный ответ: <b>' + victorinaAnswers[oldQ] +
                            '</b><br><br><b>Следующий вопрос</b>: ' + victorinaQuestions[currentQuest] + ' (' + latters + ' ' + lattersStr + ')';
                        wrongCount = 0;
                        vicBall = 3;
                        storage['quests']++;
                        storage.users[rec.s.profile.login] = score;
                    }

                }
            } else {
                rec.text = 'Вопрос не задан';
            }

            dataProvider.testovich.set(storage);
            rec.toAll = true;
            rec.save = false;
            rec.isBot = true;
            callback(null, rec);

        });

    }

    function makeHelp1(str) {
        var ret = str[0] + ' ';
        for (var i = 1; i < str.length; i++) {
            ret += '_ ';
        }
        return ret;
    }

    function makeHelp2(str) {
        var ret = str[0] + ' ';
        for (var i = 1; i < str.length - 1; i++) {
            ret += '_ ';
        }
        return ret + str[str.length - 1];
    }

    callback(null, rec);
}

function Antimat(rec){

    var str = rec.text;
    var placeholder = '***';
    //str = str.replace(new RegExp("([^A-ZА-Я])", "gi"), ' ');  // чистим все небуквенные символы
    str = str.replace(new RegExp('\\s+', 'gi'), ' ');

    if(!str) {
        rec.text = '';
        return rec;
    }

    var words = _.map(str.split(' '), function(word){

        var oldWord = word;
        antiRegs.regs.forEach(function(reg){
            word = word.replace(reg, placeholder);
        });

        if(word.indexOf(placeholder) > -1) {
            var testResult = false;
            antiRegs.exeptions.forEach(function(exp){
                if(exp.test(oldWord))
                    testResult = true;
            });
            if(testResult)
                word = oldWord;
        }

        return word;

    });

    rec.text = words.join(' ');
    return rec;

};

function nl2br(str) {
    return str.replace(/([^>])\n/g, '$1<br/>');
}

function stripBB(str) {
    return str.replace(/\[[^\]]+\]/g, '')
}

function trim(b, a) {
    return ltrim(rtrim(b, a), a)
}
function ltrim(b, a) {
    return b.replace(RegExp("^[" + (a || "\\s") + "]+", "g"), "")
}
function rtrim(b, a) {
    return b.replace(RegExp("[" + (a || "\\s") + "]+$", "g"), "")
}

function getPage(pageurl, callback) {
    var options = {
        host: url.parse(pageurl).host,
        port: 80,
        path: url.parse(pageurl).pathname
    };
    var text = '';
    http.get(options, function (res) {
        res.on('data', function (data) {
            text += data;
        }).on('end', function () {
            callback(text);
        });
    });
}
function sklonen(val, s1, s2, s5){
    var v = val % 10;
    if(v == 1) {
        return s1;
    } else if(v >= 5 || v == 0) {
        return s5;
    } else {
        return s2;
    }
}