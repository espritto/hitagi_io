var crypto = require('crypto');
var request = require('request');
var querystring = require('querystring');
var UAparse = require('user-agent-parser');

function base64_decode(f){var k,h,e,g,l,m=0,s="";do k="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(f.charAt(m++)),h="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(f.charAt(m++)),g="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(f.charAt(m++)),l="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".indexOf(f.charAt(m++)),e=k<<18|h<<12|g<<6|l,k=e>>16&255,h=e>>8&255,e&=255,s=64==g?s+String.fromCharCode(k):
64==l?s+String.fromCharCode(k,h):s+String.fromCharCode(k,h,e);while(m<f.length);return s}

function date(f,k){var h,e,g=/\\?([a-z])/gi,l,m=function(e,f){return(e+="").length<f?Array(++f-e.length).join("0")+e:e},s="Sun Mon Tues Wednes Thurs Fri Satur January February March April May June July August September October November December".split(" ");l=function(f,g){return e[f]?e[f]():g};e={d:function(){return m(e.j(),2)},D:function(){return e.l().slice(0,3)},j:function(){return h.getDate()},l:function(){return s[e.w()]+"day"},N:function(){return e.w()||7},S:function(){var f=e.j();return 4<
f&&21>f?"th":{1:"st",2:"nd",3:"rd"}[f%10]||"th"},w:function(){return h.getDay()},z:function(){var f=new Date(e.Y(),e.n()-1,e.j()),g=new Date(e.Y(),0,1);return Math.round((f-g)/864E5)+1},W:function(){var f=new Date(e.Y(),e.n()-1,e.j()-e.N()+3),g=new Date(f.getFullYear(),0,4);return m(1+Math.round((f-g)/864E5/7),2)},F:function(){return s[6+e.n()]},m:function(){return m(e.n(),2)},M:function(){return e.F().slice(0,3)},n:function(){return h.getMonth()+1},t:function(){return(new Date(e.Y(),e.n(),0)).getDate()},
L:function(){return 1===(new Date(e.Y(),1,29)).getMonth()|0},o:function(){var f=e.n(),g=e.W();return e.Y()+(12===f&&9>g?-1:1===f&&9<g)},Y:function(){return h.getFullYear()},y:function(){return(e.Y()+"").slice(-2)},a:function(){return 11<h.getHours()?"pm":"am"},A:function(){return e.a().toUpperCase()},B:function(){var e=3600*h.getUTCHours(),f=60*h.getUTCMinutes(),g=h.getUTCSeconds();return m(Math.floor((e+f+g+3600)/86.4)%1E3,3)},g:function(){return e.G()%12||12},G:function(){return h.getHours()},h:function(){return m(e.g(),
2)},H:function(){return m(e.G(),2)},i:function(){return m(h.getMinutes(),2)},s:function(){return m(h.getSeconds(),2)},u:function(){return m(1E3*h.getMilliseconds(),6)},e:function(){throw"Not supported (see source code of date() for timezone on how to add support)";},I:function(){var f=new Date(e.Y(),0),g=Date.UTC(e.Y(),0),h=new Date(e.Y(),6),k=Date.UTC(e.Y(),6);return 0+(f-g!==h-k)},O:function(){var e=h.getTimezoneOffset(),f=Math.abs(e);return(0<e?"-":"+")+m(100*Math.floor(f/60)+f%60,4)},P:function(){var f=
e.O();return f.substr(0,3)+":"+f.substr(3,2)},T:function(){return"UTC"},Z:function(){return 60*-h.getTimezoneOffset()},c:function(){return"Y-m-d\\Th:i:sP".replace(g,l)},r:function(){return"D, d M Y H:i:s O".replace(g,l)},U:function(){return h.getTime()/1E3|0}};this.date=function(e,f){h="undefined"===typeof f?new Date:f instanceof Date?new Date(f):new Date(1E3*f);return e.replace(g,l)};return this.date(f,k)}

function timeFormat(f){return date(config.timeFormat,f)}function dateFormat(f){return date(config.dateFormat,f)}var hex_chr="0123456789abcdef";function rhex(f){str="";for(j=0;3>=j;j++)str+=hex_chr.charAt(f>>8*j+4&15)+hex_chr.charAt(f>>8*j&15);return str}


exports.date2 = date;
exports.sha1 = function(str){
	return crypto.createHash('sha1').update(str).digest("hex");
};
exports.md5 = function(str){
	return crypto.createHash('md5').update(str).digest("hex");
};
exports.timeFormat = timeFormat;
exports.dateFormat = dateFormat;
exports.base64 = function(str){
    return new Buffer(str).toString('base64');
};
exports.projectPath = function(){
	var path = process.cwd();
	return path.replace(/\\/g, '/');
};

exports.createToken = function(){
	return crypto.randomBytes(24).toString('hex');
};

exports.getDeviceByUA = function(str){
	var info = UAparse(str);
	return info.os.name == 'Android' ? info.os.name : info.browser.name;
};

exports.authProfileByToken = function(token, callback){

	var formData = querystring.stringify({
		id_token: token
	});

	request({
		url: 'https://redspirit.eu.auth0.com/tokeninfo',
		method: "POST",
		json: true,
		headers: {
			'Content-Length': formData.length,
			"content-type": "application/x-www-form-urlencoded"
		},
		body: formData
	}, function(err, response){

		if(response.statusCode != 200) {
			callback({
				code: response.statusCode,
				message: response.body
			});
		} else {
			callback(null, response.body);
		}

	});

};

exports.socialDataByProvider = function(profile){

	// var r = {
	//     "name":"aniwatch",
	//     "family_name":"Таянчин",
	//     "given_name":"Алексей",
	//     "picture":"https://pp.vk.me/c636829/v636829439/193db/shYRIMuegyQ.jpg",
	//     "gender":"male",
	//     "clientID":"oHTGcfXKEWjBACwwgAm5bga9Qe92XJLA",
	//     "updated_at":"2016-12-03T21:41:44.600Z",
	//     "user_id":"vkontakte|172144439",
	//     "nickname":"aniwatch",
	//     "identities":[{"provider":"vkontakte","user_id":172144439,"connection":"vkontakte","isSocial":true}],
	//     "created_at":"2016-12-03T10:34:30.876Z",
	//     "global_client_id":"4wCFWGreo2phzTWNYszolruAV0HTBvlS"
	// };

	var identities = profile.identities[0];
	var socialData = {
		gender: profile.gender == 'male' ? 1 : (profile.gender == 'female' ? 2 : 0)
	};

	if(identities.provider == 'vkontakte') {

		socialData.type = 'vk';
		socialData.vkId = identities.user_id;
		socialData.login = "u" + identities.user_id;
		socialData.nick = profile.given_name ? profile.given_name : profile.nickname;
		socialData.realName =  profile.given_name + ' ' + profile.family_name;

	} else if(identities.provider == 'google-oauth2') {

		socialData.type = 'google';
		socialData.login = "g" + identities.user_id;
		socialData.nick = profile.given_name ? profile.given_name : profile.nickname;
		socialData.realName =  profile.given_name;

	} else {
		return null;
	}

	return socialData;

};