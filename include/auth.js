var config = require('../configs/config.js');
var rooms = require('./rooms.js');
var tools = require('./tools.js');
var sessions = require('./sessions.js');
var _ = require('underscore');


function authByToken(param) {
    var socket = this;
    var token = pWrap(param['token']);
    var clientStr = pWrap(param['client']);
    var platform = pWrap(param['platform']);

    sessions.userByToken(token, function(err, user){

        if (!user)
            return socket.emit('auth', {status: 'error', reason: 'wrong token', message: 'Неверный токен'});

        if (user.block == 1)
            return socket.emit('auth', {status: 'error', reason: 'userblocked', message: user.block_reason});

        sessions.update(user.currentSession._id, {
            client: clientStr,
            platform: platform,
            ip: socket.client_ip
        });

        //allUsers[user['login']] = socket;

        socket.isLogin = true;
        socket.loginMode = 'user';
        socket.session = user.currentSession;
        socket.profile.login = user.login;
        socket.profile._id = user._id;
        socket.profile.nick = user.nick;
        socket.profile.privilege = user.privilege;
        socket.profile.avaindex = user.ava_index;
        socket.profile.statustext = user.statustext;
        socket.profile.state = user.state;
        socket.profile.voiceoff = user.voiceoff;
        socket.profile.profvisible = user.profile_visible;
        socket.profile.nickdate = isset(user.nick_date) ? user.nick_date : 0;
        socket.profile.lastmess = {};
        socket.profile.avaurl = user.ava_index ? '/avatars/' + user.login + '.jpg?' + user.ava_index : '/avatars/noavatar.gif';
        socket.profile.awayStatus = 0;

        login2nick[user.login] = user.nick;

        if (!isset(user.textcolor)) user.textcolor = '000000';
        socket.emit('auth', {
            status: 'ok',
            login: user.login,
            privilege: user.privilege,
            nickname: user.nick,
            statustext: user.statustext,
            state: user.state,
            textcolor: user.textcolor,
            url: socket.profile.avaurl
        });

        socket.join('pm_' + user.login);

    });

}

function logInUser(param) {
    var socket = this;
    var login = pWrap(param['login']);
    var pass = pWrap(param['pass']);
    var clientStr = pWrap(param['client']);
    var platform = pWrap(param['platform']);

    dbusers.findOne({login: login, pass: pass}, function (err, res) {
        if(!res)
            return sendError('wrongauth', socket);

        if (res['block'] == 1)
            return socket.emit('login', {status: 'error', reason: 'userblocked', message: res['block_reason']});

        if(res['linked_to'])
            return dbusers.findOne({_id: res['linked_to']}, function (err2, res2) {
                if(!res2)
                    return sendError('wrongauth', socket);
                if (res2['block'] == 1)
                    return socket.emit(login, {status: 'error', reason: 'userblocked', message: res2['block_reason']});
                processUser(res2);
            });

        processUser(res);

    });

    var processUser = function(user){

        sessions.create(user._id, {
            client: clientStr,
            platform: platform,
            ip: socket.client_ip
        }, function(err, session){

            socket.isLogin = true;
            socket.loginMode = 'user';
            socket.session = session;
            socket.profile.login = user.login;
            socket.profile._id = user._id;
            socket.profile.nick = user.nick;
            socket.profile.privilege = user.privilege;
            socket.profile.avaindex = user.ava_index;
            socket.profile.statustext = user.statustext;
            socket.profile.state = user.state;
            socket.profile.profvisible = user.profile_visible;
            socket.profile.nickdate = isset(user.nick_date) ? user.nick_date : 0;
            socket.profile.lastmess = {};
            socket.profile.avaurl = user.ava_index ? '/avatars/' + login + '.jpg?' + user.ava_index : '/avatars/noavatar.gif';
            socket.profile.awayStatus = 0;

            login2nick[login] = user.nick;

            if (!isset(user.textcolor)) user.textcolor = '000000';
            socket.emit('login', {
                status: 'ok',
                login: login,
                token: session.token,
                privilege: user.privilege,
                nickname: user.nick,
                statustext: user.statustext,
                state: user.state,
                textcolor: user.textcolor,
                url: socket.profile.avaurl
            });

            socket.join('pm_' + login);

        });

        //allUsers[res['login']] = socket;

    }

}

function authSocial(param) {
    var socket = this;
    var socToken = pWrap(param['token']);
    var platform = pWrap(param['platform']);
    var clientStr = pWrap(param['client']);

    tools.authProfileByToken(socToken, function(err, profile){

        if(err)
            return socket.emit('loginsocial', {status: 'error', reason: 'code_' + err.code, message: err.message});

        var socialData = tools.socialDataByProvider(profile);

        if(!socialData)
            return socket.emit('loginsocial', {status: 'error', reason: 'invalid provider' + err.code, message: ''});

        dbusers.findOne({login: socialData.login}, function (err, res) {

            // если такой юзер уже есть
            if(res) {

                if (res.block == 1)
                    return socket.emit('loginsocial', {status: 'error', reason: 'userblocked', message: res.block_reason});

                if(res.linked_to)
                    return dbusers.findOne({_id: res.linked_to}, function (err2, res2) {
                        if(!res2)
                            return socket.emit('loginsocial', {status: 'error', reason: 'Linked user not found', message: ''});
                        if (res2.block == 1)
                            return socket.emit('loginsocial', {status: 'error', reason: 'userblocked', message: res.block_reason});
                        processUser(res2);
                    });

                return processUser(res);

            }

            // если юзера нет, регаем его

            var newUser = {
                login: socialData.login,
                nick: socialData.nick,
                pass: '-',
                reg_date: time(),
                statustext: '',
                state: 0,
                privilege: 2,
                gender: socialData.gender,
                block_reason: '',
                ava_index: 0,
                vk: socialData.type == 'vk' ? 'http://vk.com/id' + socialData.vkId : '',
                realname: socialData.realName,
                rating: 0,
                mess_count: 0,
                textcolor: '#000',
                block: 0,
                profile_visible: 1,
                social_type: socialData.type
            };

            dbusers.insert(newUser, function (err, regres) {

                return authSocial.call(socket, {
                    token: socToken,
                    client: clientStr,
                    platform: platform
                });

            });

        });

    });

    var processUser = function(user){

        //allUsers[res['login']] = socket;

        sessions.create(user._id, {
            client: clientStr,
            platform: platform,
            ip: socket.client_ip
        }, function(err, session){

            socket.isLogin = true;
            socket.loginMode = 'user';
            socket.session = session;
            socket.profile._id = user._id;
            socket.profile.login = user.login;
            socket.profile.nick = user.nick;
            socket.profile.privilege = user.privilege;
            socket.profile.avaindex = user.ava_index;
            socket.profile.statustext = user.statustext;
            socket.profile.state = user.state;
            socket.profile.profvisible = user.profile_visible;
            socket.profile.nickdate = isset(user.nick_date) ? user.nick_date : 0;
            socket.profile.lastmess = {};
            socket.profile.avaurl = user.ava_index ? '/avatars/' + user.login + '.jpg?' + user.ava_index : '/avatars/noavatar.gif';
            socket.profile.awayStatus = 0;

            login2nick[user.login] = user.nick;

            if (!isset(user.textcolor)) user.textcolor = '000000';

            socket.join('pm_' + user.login);

            socket.emit('loginsocial', {
                status: 'ok',
                login: user.login,
                token: session.token,
                privilege: user.privilege,
                nickname: user.nick,
                statustext: user.statustext,
                state: user.state,
                textcolor: user.textcolor,
                url: socket.profile.avaurl
            });

        });

    }

}

function linkSocial(param){
    var socket = this;
    var socToken = pWrap(param['token']);

    tools.authProfileByToken(socToken, function(err, profile){
        if(err)
            return socket.emit('linksocial', {status: 'error', reason: 'code_' + err.code, message: err.message});

        var socialData = tools.socialDataByProvider(profile);

        if(!socialData)
            return socket.emit('linksocial', {status: 'error', reason: 'invalid provider' + err.code, message: ''});

        dbusers.findOne({login: socialData.login}, function (err, res) {

            // юзер которого привязывает уже существует
            if(res) {

                if (res.block == 1)
                    return socket.emit('linksocial', {status: 'error', reason: 'userblocked', message: res['block_reason']});


                dbusers.updateById(res._id, {
                    $set: {
                        linked_to: socket.profile._id,
                        social_type: socialData.type
                    }
                }, function (err, res) {

                    socket.emit('linksocial', {
                        status: 'ok',
                        login: res.login,
                        nickname: res.nick,
                        social_type: socialData.type
                    });

                });

            } else {

                // придется создать юзера которого привязываем

                var newUser = {
                    login: socialData.login,
                    nick: socialData.nick,
                    pass: '-',
                    reg_date: time(),
                    statustext: '',
                    state: 0,
                    privilege: 2,
                    gender: socialData.gender,
                    block_reason: '',
                    ava_index: 0,
                    vk: socialData.type == 'vk' ? 'http://vk.com/id' + socialData.vkId : '',
                    realname: socialData.realName,
                    rating: 0,
                    mess_count: 0,
                    textcolor: '#000',
                    block: 0,
                    profile_visible: 1,
                    social_type: socialData.type,
                    linked_to: socket.profile._id
                };

                dbusers.insert(newUser, function (err, regres) {

                    socket.emit('linksocial', {
                        status: 'ok',
                        login: res.login,
                        nickname: res.nick,
                        social_type: socialData.type
                    });

                });

            }

        });

    });

}

function unlinkSocial(param){
    var socket = this;
    var login = pWrap(param['login']);

    dbusers.findOne({
        login: login,
        linked_to: socket.profile._id
    }, function (err, res) {
        if(!res)
            return socket.emit('unlinksocial', {status: 'error', reason: 'link_not_found', message: 'Указанный пользователь не привязан к вам'});

        dbusers.updateById(res._id, {
            $set: {
                linked_to: null
            }
        }, function (err, res) {

            socket.emit('unlinksocial', {
                status: 'ok',
                login: res.login
            });

        });

    });

}

function logOut(param) {

    var socket = this;
    if (!socket.isLogin) {
        socket.emit('logout', {status: 'error', reason: 'notlogin'});
        return false;
    }

    rooms.leaveAllRooms(socket);
    socket.leave('pm_' + socket.profile.login);

    sessions.remove(socket.session._id);

    socket.emit('logout', {'status': 'ok'});
    socket.isLogin = false;

}

function delUser(param) {
    var socket = this;
    var us = pWrap(param['user']);

    if (!socket.isLogin) {
        socket.emit('deluser', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    if (socket.profile.privilege > 1) {
        socket.emit('deluser', {'status': 'error', 'reason': 'notallow'});
        return false;
    }

    dbusers.findOne({'login': us}, function (err, res) {
        if (res) {
            if (res['privilege'] == 0) {
                socket.emit('deluser', {'status': 'error', 'reason': 'notallow'});
                return false;
            }
            dbusers.removeById(res['_id'], function (err, res) {
            });
            socket.emit('deluser', {'status': 'ok'});
        } else {
            sendError('notexists', socket);
        }
    });

}

function blockUser(param) {
    var socket = this;
    var us = pWrap(param['user']);
    var act = pWrap(param['action']);
    var rs = pWrap(param['reason']);

    if (!socket.isLogin) {
        socket.emit('blockuser', {'status': 'error', 'reason': 'notlogin'});
        return false;
    }

    if (socket.profile.privilege > 1 || socket.profile.login == us) {
        socket.emit('blockuser', {'status': 'error', 'reason': 'notallow'});
        return false;
    }

    dbusers.findOne({'login': us}, function (err, res) {
        if (res) {
            if (socket.profile.privilege == 1 && res['privilege'] == 0) {
                socket.emit('blockuser', {'status': 'error', 'reason': 'notallow'});
                return false;
            }
            dbusers.updateById(res['_id'], {$set: {'block': act, 'block_reason': rs}}, function (err, res) {
            });
            socket.emit('blockuser', {'status': 'ok'});
        } else {
            sendError('notexists', socket);
        }
    });
}

function sendError(reason, socket) {
    socket.emit('login', {status: 'error', reason: reason});
}

function sendAnotherLogin(login) {
    io.to('pm_' + login).emit('auth', {status: 'error', reason: 'anotherlogin'});
}
function pWrap(par) {
    return isset(par) ? par : '';
}

exports.authByToken = authByToken;
exports.logIn = logInUser;
exports.logOut = logOut;
exports.delUser = delUser;
exports.blockUser = blockUser;
exports.authSocial = authSocial;
exports.linkSocial = linkSocial;
exports.unlinkSocial = unlinkSocial;