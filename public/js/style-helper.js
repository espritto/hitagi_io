/**
 * Created by espri on 03.03.2017.
 */

/* обрабатываем фон */
window.addEventListener("storage", handle_storage, false);
var sheet = document.styleSheets[document.styleSheets.length - 1];
function handle_storage(ev) {

    var st = '';
    if (!localStorage.backimage1 && !localStorage.backimage2) {
        if (ev) {

            sheet.deleteRule(0);
            sheet.deleteRule(0);

            addStyle('.message-pane-wrapper', 'background: url(themes/default/fon5.png) no-repeat bottom right');
            addStyle('.roster-pane', 'background: url(themes/default/fonu1.png) no-repeat bottom right, url(themes/default/bg2.png)');

        }
        return false;
    }

    sheet.deleteRule(0);
    sheet.deleteRule(0);

    var met = localStorage.backmethod1 ? localStorage.backmethod1 : 'cover';
    var color = localStorage.backcolor1 ? localStorage.backcolor1 : '#FFFFFF';
    var data = color + ' url(' + localStorage.backimage1 + ') ';
    if (met == 'cover') st = 'no-repeat scroll 0% 100% / cover padding-box border-box';
    if (met == 'center') st = 'no-repeat 50% 50%';
    if (met == 'right-bottom') st = 'no-repeat 100% 100%';
    if (met == 'repeat') st = '';
    if (met == 'fill') st = 'no-repeat scroll 0% 0% / 100% 100%';
    addStyle('.message-pane-wrapper', 'background: ' + data + st);

    met = localStorage.backmethod2 ? localStorage.backmethod2 : 'repeat';
    color = localStorage.backcolor2 ? localStorage.backcolor2 : '#FFFFFF';
    data = color + ' url(' + localStorage.backimage2 + ') ';
    if (met == 'cover') st = 'no-repeat scroll 0% 100% / cover padding-box border-box';
    if (met == 'center') st = 'no-repeat 50% 50%';
    if (met == 'right-bottom') st = 'no-repeat 100% 100%';
    if (met == 'repeat') st = '';
    if (met == 'fill') st = 'no-repeat scroll 0% 0% / 100% 100%';
    addStyle('.roster-pane', 'background: ' + data + st);
}
function addStyle(sel, rule) {
    if (sheet.addRule) {
        sheet.addRule(sel, rule);
    }
    else {
        if (sheet.insertRule) {
            sheet.insertRule(sel + ' {' + rule + '}', sheet.cssRules.length);
        }
    }
}
handle_storage();

if (localStorage.designImages) {
    designImagesUpdate(localStorage.designImages);
} else {
    localStorage.setItem('designImages', 'normal');
    designImagesUpdate('normal');
}
if (localStorage.designTheme) {
    designThemeUpdate(localStorage.designTheme);
} else {
    localStorage.setItem('designTheme', 'theme-default');
    designThemeUpdate('theme-default');
}


function designImagesUpdate(mode) {
    if (mode == 'normal') $('.inlinepic').css({'max-height': '600px', 'max-width': '600px', 'display': 'inline'});
    if (mode == 'small') $('.inlinepic').css({'max-height': '200px', 'max-width': '200px', 'display': 'inline'});
    if (mode == 'hide') $('.inlinepic').css({'display': 'none'});
    $('.pic-block').css('height', 'auto');
    localStorage.setItem('designImages', mode);
}
function designThemeUpdate(mode) {
    $('.my-styles').attr('href', 'themes/default/' + mode + '.css');
    localStorage.setItem('designTheme', mode);
}

$('.head-trigger').click(headTriggerClick);
function headTriggerClick() {
    $(this).toggleClass('ht-close');
    $('#cont').toggleClass('head-hide');
    localStorage.headHide = localStorage.headHide == '1' ? '0' : '1';
}
if (localStorage.headHide == '1') headTriggerClick();